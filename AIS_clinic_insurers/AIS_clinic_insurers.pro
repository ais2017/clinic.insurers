QT += core
QT -= gui

QMAKE_CXXFLAGS += -std=c++0x

CONFIG += c++11

CONFIG += qtestlib
TARGET = ../TestLib

SOURCES += \
    class_clinic_insurers.cpp \
    test.cpp
HEADERS = class_clinic_insurers.h
CONFIG += qtestlib
TARGET = ../TestLib
