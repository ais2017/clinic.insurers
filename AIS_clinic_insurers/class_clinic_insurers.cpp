#include "class_clinic_insurers.h"

//Policy
Policy::Policy(QString PNunber, QString PPatient, QDate PTillDate)
    : number(PNunber), patient(PPatient), TillDate(PTillDate)
{}

bool Policy::isValid(){
    if (TillDate < QDate::currentDate())
        return true;
    return false;
}

QString Policy::GetNumber(){
    return number;
}

QString Policy::GetPatient(){
    return patient;
}

QDate Policy::GetTillDate(){
    return TillDate;
}



//InsuranceCompany
InsuranceCompany::InsuranceCompany(QString ICName, QString ICAddress, QString ICPhonenum)
    : name(ICName), address(ICAddress), phonenum(ICPhonenum)
{}

void InsuranceCompany::UpdateAddress(QString newAddress){
    address = newAddress;
    return;
}

void InsuranceCompany::UpdatePhonenumber(QString newPhonenumber){
    phonenum = newPhonenumber;
    return;
}

QString InsuranceCompany::GetName(){
    return name;
}

QString InsuranceCompany::GetAddress(){
    return address;
}

QString InsuranceCompany::GetPhonenumber(){
    return phonenum;
}



//Service
Service::Service(int SNumber, QString SName, QString SInfo, int SCost)
    : number(SNumber), name(SName), info(SInfo), cost(SCost)
{}

int Service::GetNumber(){
    return number;
}

QString Service::GetName(){
    return name;
}

QString Service::GetInfo(){
    return info;
}

int Service::GetCost(){
   return cost;
}



//Emploee
Emploee::Emploee(int ENumber, QString EPosition, QString EFIO)
    : number(ENumber), position(EPosition), FIO(EFIO)
{}

int Emploee::GetNumber(){
    return number;
}

QString Emploee::GetPosition(){
    return position;
}

QString Emploee::GetFIO(){
    return FIO;
}



//Patient
Patient::Patient(int PNumber, QString PFIO, QString PPassport)
    : number(PNumber), FIO(PFIO), passport(PPassport)
{}

void Patient::AddPolicy(Policy *P_policy){
    if (P_policy){
        ListOfPolicys[P_policy->GetNumber()] = P_policy;
    } else
        throw std::exception();
}

 QMap<QString, Policy *> &Patient::GetPolicys(){
    return ListOfPolicys;
}

Policy *Patient::GetPolicy(QString PolicyNumber){
    if(ListOfPolicys.contains(PolicyNumber))
            return ListOfPolicys[PolicyNumber];

    throw std::exception();
    return NULL;
}

int Patient::GetNumber(){
    return number;
}

QString Patient::GetFIO(){
    return FIO;
}

QString Patient::GetPassport(){
    return passport;
}



//InsuranceProgram
InsuranceProgram::InsuranceProgram(int ICNumber, QString ICInfo)
    : number(ICNumber), info(ICInfo)
{}

void InsuranceProgram::IssuePolicy(Policy *newPolicy){
    if (newPolicy){
        ListOfPolicys[newPolicy->GetNumber()] = newPolicy;
        return;
    }

    throw std::exception();
}

QMap<QString, Service *> &InsuranceProgram::GetServices(){
    return ListOfServices;
}

QMap<QString, Policy *> &InsuranceProgram::GetPolicys(){
    return ListOfPolicys;
}

void InsuranceProgram::AddService(Service *newService){
    if (newService){
        ListOfServices[newService->GetName()] = newService;
        return;
    }

    throw std::exception();
}

void InsuranceProgram::RemoveService(QString SName){
    if(ListOfServices.contains(SName))
        ListOfServices.remove(SName);
    else
        throw std::exception();
}

Service *InsuranceProgram::GetService(QString SName){
    if(ListOfServices.contains(SName))
        return ListOfServices[SName];

    throw std::exception();
    return NULL;
}

Policy *InsuranceProgram::GetPolicy(QString PNumber){
    if(ListOfPolicys.contains(PNumber))
        return ListOfPolicys[PNumber];

    throw std::exception();
    return NULL;
}

int InsuranceProgram::GetNumber(){
    return number;
}

QString InsuranceProgram::GetInfo(){
    return info;
}



//Condition
Condition::Condition(int CNumber, int ContractNumber, QString CInfo)
    : number(CNumber), ContractNumber(ContractNumber), info(CInfo)
{}

int Condition::GetNumber(){
    return number;
}

int Condition::GetContractNumber(){
    return ContractNumber;
}

QString Condition::GetInfo(){
    return info;
}



//Contract
Contract::Contract(int CNumber, Emploee *CManager, InsuranceCompany *CICName, QDate CFromDate, QDate CToDate, QString CInfo)
    : number(CNumber), manager(CManager), IC(CICName), FromDate(CFromDate), ToDate(CToDate), info(CInfo), status("New")
{
    if (CFromDate > CToDate)
        throw std::exception();
}

void Contract::AddCondition(Condition *newCondition){
    if (newCondition){
        ListOfConditions[newCondition->GetNumber()] = newCondition;
        return;
    }
    throw std::exception();
}

void Contract::AddInsuranceProgram(InsuranceProgram *newInsuranceProgram){
    if (newInsuranceProgram){
        ListOfInsurancePrograms[newInsuranceProgram->GetNumber()] = newInsuranceProgram;
        return;
    }
    throw std::exception();
}

void Contract::RemoveInsuranceProgram(int IPNumber){
    if(ListOfInsurancePrograms.contains(IPNumber))
        ListOfInsurancePrograms.remove(IPNumber);
    else
        throw std::exception();
}

void Contract::TerminateContract(){
    status = "Terminated";
}

void Contract::SetToDate(QDate newToDate){
    if(newToDate > FromDate)
        ToDate = newToDate;
    else
       throw std::exception();
}

QMap<int, InsuranceProgram *> &Contract::GetInsurancePrograms(){
    return ListOfInsurancePrograms;
}

InsuranceProgram *Contract::GetInsuranceProgram(int IPNumber){
    if(ListOfInsurancePrograms.contains(IPNumber))
        return ListOfInsurancePrograms[IPNumber];

    throw std::exception();
    return NULL;
}

void Contract::Approve(){
    if (status == "Terminated")
        return;

    if (ToDate < QDate::currentDate()){
        status = "Expired";
        return;
    }

    if ((ListOfConditions.size() == 0) || (ListOfInsurancePrograms.size() == 0) ){
        status = "New";
    } else {
        status = "Active";
    }
}

Condition *Contract::GetCondition(int ConditionNumber){
    if(ListOfConditions.contains(ConditionNumber))
        return ListOfConditions[ConditionNumber];

    throw std::exception();
    return NULL;
}

QMap<int, Condition *> &Contract::GetConditions(){
    return ListOfConditions;
}

int Contract::GetNumber(){
    return number;
}

Emploee *Contract::GetManager(){
    return manager;
}

InsuranceCompany *Contract::GetInsuranceCompany(){
    return IC;
}

QDate Contract::GetFromDate(){
    return FromDate;
}

QDate Contract::GetToDate(){
    return ToDate;
}

QString Contract::GetInfo(){
    return info;
}

QString Contract::GetStatus(){
    return status;
}
