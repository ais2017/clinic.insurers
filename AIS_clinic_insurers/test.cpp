#include <QtTest>
#include <QDate>
#include "class_clinic_insurers.h"
#include <QObject>
#include <typeinfo>

// ======================================================================
class Test : public QObject {
    Q_OBJECT

private slots:
    void Policy_isValid();

    void Patient_AddPolicy();
    void Patient_GetPolicy();

    void InsuranceProgram_IssuePolicy();
    void InsuranceProgram_AddService();
    void InsuranceProgram_RemoveService();

    void InsuranceProgram_GetService();
    void InsuranceProgram_GetPolicy();

    void Contract_AddInsuranceProgram();
    void Contract_RemoveInsuranceProgram();
    void Contract_Approve();
    void Contract_AddCondition();
    void Contract_GetInsuranceProgram();
    void Contract_GetCondition();
    void Contract_Date();
};

// ----------------------------------------------------------------------
void Test::Policy_isValid(){

    Policy policy("PNumber_1", "PPatient_1", QDate::currentDate());
    QCOMPARE(policy.isValid(), false);
    QCOMPARE(policy.isValid(), false);


}

void Test::Patient_AddPolicy(){
    Patient patient(1, "PFIO", "PPassport");
    Policy policy_1("PNumber_1", "PPatient_1", QDate::currentDate());
    Policy policy_2("PNumber_2", "PPatient_1", QDate::currentDate());
    Policy *policy_3(NULL);

    patient.AddPolicy(&policy_1);
    patient.AddPolicy(&policy_2);
    QVERIFY_EXCEPTION_THROWN(patient.AddPolicy(policy_3), std::exception);

    QCOMPARE(patient.GetPolicy("PNumber_1"),&policy_1);
    QCOMPARE(patient.GetPolicy("PNumber_2"),&policy_2);
    QVERIFY_EXCEPTION_THROWN(patient.GetPolicy("PNumber_3"), std::exception);
}

void Test::Patient_GetPolicy(){
    Patient patient(1, "PFIO", "PPassport");
    Policy policy_1("PNumber_1", "PPatient_1", QDate::currentDate());
    Policy policy_2("PNumber_2", "PPatient_1", QDate::currentDate());
    Policy policy_3("PNumber_3", "PPatient_3", QDate::currentDate());
    patient.AddPolicy(&policy_1);
    patient.AddPolicy(&policy_2);

    QCOMPARE(patient.GetPolicy("PNumber_1"),&policy_1);
    QCOMPARE(patient.GetPolicy("PNumber_2"),&policy_2);
    QVERIFY_EXCEPTION_THROWN(patient.GetPolicy("PNumber_3"), std::exception);

}

void Test::InsuranceProgram_IssuePolicy(){
    InsuranceProgram insuranceprogram(1, "ICInfo");
    Policy policy_1("PNumber_1", "PPatient_1", QDate::currentDate());
    Policy policy_2("PNumber_2", "PPatient_1", QDate::currentDate());
    Policy *policy_3(NULL);

    insuranceprogram.IssuePolicy(&policy_1);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.IssuePolicy(policy_3), std::exception);
    insuranceprogram.IssuePolicy(&policy_2);


    QCOMPARE(insuranceprogram.GetPolicy("PNumber_1"),&policy_1);
    QCOMPARE(insuranceprogram.GetPolicy("PNumber_2"),&policy_2);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetPolicy("PNumber_3"), std::exception);
}

void Test::InsuranceProgram_AddService(){
    InsuranceProgram insuranceprogram(1, "ICInfo");
    Service service_1(1, "SName_1", "SInfo", 100);
    Service service_2(2, "SName_2", "SInfo", 100);
    Service *service_3(NULL);

    insuranceprogram.AddService(&service_1);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.AddService(service_3), std::exception);
    insuranceprogram.AddService(&service_2);

    QCOMPARE(insuranceprogram.GetService("SName_1"),&service_1);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetService("service_3"), std::exception);
    QCOMPARE(insuranceprogram.GetService("SName_2"),&service_2);
}

void Test::InsuranceProgram_RemoveService(){
    InsuranceProgram insuranceprogram(1, "ICInfo");
    Service service_1(1, "SName_1", "SInfo", 100);
    Service service_2(2, "SName_2", "SInfo", 100);
    Service service_3(2, "SName_3", "SInfo", 100);

    insuranceprogram.AddService(&service_1);
    insuranceprogram.AddService(&service_2);
    insuranceprogram.AddService(&service_3);

    insuranceprogram.RemoveService("SName_2");
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetService("SName_2"), std::exception);

    QVERIFY_EXCEPTION_THROWN(insuranceprogram.RemoveService("SName_2"), std::exception);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.RemoveService("SName_4"), std::exception);

    insuranceprogram.RemoveService("SName_1");
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetService("SName_1"), std::exception);
}

void Test::InsuranceProgram_GetService(){
    InsuranceProgram insuranceprogram(1, "ICInfo");
    Service service_1(1, "SName_1", "SInfo", 100);
    Service service_2(2, "SName_2", "SInfo", 100);
    Service service_3(3, "SName_3", "SInfo", 100);

    insuranceprogram.AddService(&service_1);
    insuranceprogram.AddService(&service_2);

    QCOMPARE(insuranceprogram.GetService("SName_1"),&service_1);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetService("service_3"), std::exception);
    QCOMPARE(insuranceprogram.GetService("SName_2"),&service_2);

}

void Test::InsuranceProgram_GetPolicy(){
    InsuranceProgram insuranceprogram(1, "ICInfo");
    Policy policy_1("PNumber_1", "PPatient_1", QDate::currentDate());
    Policy policy_2("PNumber_2", "PPatient_1", QDate::currentDate());
    Policy policy_3("PNumber_3", "PPatient_3", QDate::currentDate());
    insuranceprogram.IssuePolicy(&policy_1);
    insuranceprogram.IssuePolicy(&policy_2);


    QCOMPARE(insuranceprogram.GetPolicy("PNumber_1"),&policy_1);
    QVERIFY_EXCEPTION_THROWN(insuranceprogram.GetPolicy("PNumber_3"), std::exception);
    QCOMPARE(insuranceprogram.GetPolicy("PNumber_2"),&policy_2);
}

void Test::Contract_AddInsuranceProgram(){
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, QDate::currentDate(), QDate::currentDate(), "CInfo");

    InsuranceProgram insuranceprogram_1(1, "ICInfo");
    InsuranceProgram insuranceprogram_2(2, "ICInfo");
    InsuranceProgram *insuranceprogram_3(NULL);

    contract.AddInsuranceProgram(&insuranceprogram_1);
    QVERIFY_EXCEPTION_THROWN(contract.AddInsuranceProgram(insuranceprogram_3), std::exception);
    contract.AddInsuranceProgram(&insuranceprogram_2);

    QCOMPARE(contract.GetInsuranceProgram(1), &insuranceprogram_1);
    QVERIFY_EXCEPTION_THROWN(contract.GetInsuranceProgram(3), std::exception);
    QCOMPARE(contract.GetInsuranceProgram(2),&insuranceprogram_2);
}

void Test::Contract_RemoveInsuranceProgram(){
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, QDate::currentDate(), QDate::currentDate(), "CInfo");

    InsuranceProgram insuranceprogram_1(1, "ICInfo1");
    InsuranceProgram insuranceprogram_2(2, "ICInfo2");
    InsuranceProgram insuranceprogram_3(3, "ICInfo3");

    contract.AddInsuranceProgram(&insuranceprogram_1);
    contract.AddInsuranceProgram(&insuranceprogram_2);
    contract.AddInsuranceProgram(&insuranceprogram_3);

    contract.RemoveInsuranceProgram(1);
    QVERIFY_EXCEPTION_THROWN(contract.GetInsuranceProgram(1), std::exception);

    QVERIFY_EXCEPTION_THROWN(contract.RemoveInsuranceProgram(1), std::exception);
    QVERIFY_EXCEPTION_THROWN(contract.RemoveInsuranceProgram(4), std::exception);

    contract.RemoveInsuranceProgram(2);
    QVERIFY_EXCEPTION_THROWN(contract.GetInsuranceProgram(2), std::exception);
}

void Test::Contract_Approve(){
    QDate d(2018,11,11);
    QString status = "New";
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, d, QDate::currentDate(),"CInfo");
    InsuranceProgram insuranceprogram_1(1, "ICInfo");
    Condition condition_1(1, 1, "CInfo");

    contract.Approve();
    QCOMPARE(contract.GetStatus(),status);

    contract.AddInsuranceProgram(&insuranceprogram_1);
    contract.AddCondition(&condition_1);
    status = "Active";

    contract.Approve();
    QCOMPARE(contract.GetStatus(),status);

    d.setDate(2018,11,18);
    contract.SetToDate(d);
    status = "Expired";

    contract.Approve();
    QCOMPARE(contract.GetStatus(),status);

    contract.TerminateContract();
    status = "Terminated";

    contract.Approve();
    QCOMPARE(contract.GetStatus(),status);

}

void Test::Contract_Date(){
    QDate d;
    QString status = "Expired";
    d.setDate(2018,11,11);
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    QVERIFY_EXCEPTION_THROWN(
               Contract contract(1, &manager, &insuranceprogram, QDate::currentDate(), d, "CInfo"), std::exception);
}

void Test::Contract_AddCondition(){
    QDate d;
    QString status = "Expired";
    d.setDate(2018,11,11);
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, d, QDate::currentDate(), "CInfo");
    Condition condition_1(1, 1, "CInfo");
    Condition condition_2(2, 1, "CInfo");
    Condition *condition_3(NULL);

    contract.AddCondition(&condition_1);
    QVERIFY_EXCEPTION_THROWN(contract.AddCondition(condition_3), std::exception);
    contract.AddCondition(&condition_2);

    QCOMPARE(contract.GetCondition(1), &condition_1);
    QVERIFY_EXCEPTION_THROWN(contract.GetCondition(3), std::exception);
    QCOMPARE(contract.GetCondition(2), &condition_2);
}

void Test::Contract_GetInsuranceProgram(){
    QDate d;
    QString status = "Expired";
    d.setDate(2018,11,11);
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, d, QDate::currentDate(),"CInfo");
    InsuranceProgram insuranceprogram_1(1, "ICInfo");
    InsuranceProgram insuranceprogram_2(2, "ICInfo");
    InsuranceProgram insuranceprogram_3(3, "ICInfo");

    contract.AddInsuranceProgram(&insuranceprogram_1);
    contract.AddInsuranceProgram(&insuranceprogram_2);
    contract.AddInsuranceProgram(&insuranceprogram_3);

    QCOMPARE(contract.GetInsuranceProgram(2), &insuranceprogram_2);
    QVERIFY_EXCEPTION_THROWN(contract.GetInsuranceProgram(4), std::exception);
    QCOMPARE(contract.GetInsuranceProgram(3), &insuranceprogram_3);
}

void Test::Contract_GetCondition(){
    QDate d;
    QString status = "Expired";
    d.setDate(2018,11,11);
    Emploee manager(1, "EPosition", "EFIO");
    InsuranceCompany insuranceprogram("ICName", "ICAddress", "ICPhonenum");
    Contract contract(1, &manager, &insuranceprogram, d, QDate::currentDate(), "CInfo");
    Condition condition_1(1, 1, "CInfo");
    Condition condition_2(2, 1, "CInfo");
    Condition condition_3(3, 1, "CInfo");

    contract.AddCondition(&condition_1);
    contract.AddCondition(&condition_2);
    contract.AddCondition(&condition_3);

    QCOMPARE(contract.GetCondition(2),&condition_2);
    QVERIFY_EXCEPTION_THROWN(contract.GetCondition(4), std::exception);;
    QCOMPARE(contract.GetCondition(1),&condition_1);
}

QTEST_MAIN(Test)
#include "test.moc"
