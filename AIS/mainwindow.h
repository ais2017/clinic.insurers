#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "addcontract.h"
#include "contractinfo.h"
#include "addic.h"
#include "viewlistofic.h"
#include "businses_logic/gateway.h"
#include "businses_logic/business_component.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(int manager_number, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();

    void on_tableWidget_cellDoubleClicked(int row, int column);

    void on_pushButton_2_clicked();

    void newContractinfo(Contract_info *Ci);
    void newInsuranceCompanyinfo(InsuranceCompany_info *ICi);
    void newcondition(Condition_info *Coni);
    void TerminateContract(Contract_info *Ci);
    void listICs();
private:
    Ui::MainWindow *ui;

    AddIC *addIC;
    AddContract *addC;
    ViewListOfIC *viewIC;
    ContractInfo *viewC;

    QMap<int, Contract_info*> ListOfContractsInfo;
    int manager_number;

    DataBase_Component DBC;
    Clinic_Patient_Component CPnC;
    Clinic_Personal_Component CPrC;
    Business_component *BC;
};

#endif // MAINWINDOW_H
