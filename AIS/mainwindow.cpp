#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(int manager_number, QWidget *parent) :
    manager_number(manager_number), QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    BC = new Business_component(&DBC,&CPnC,&CPrC);

    addIC = new AddIC(this);
    ui->action_2->connect(ui->action_2,SIGNAL(triggered()),addIC,SLOT(exec()));
    connect(addIC, SIGNAL(newInsuranceCompanyinfo(InsuranceCompany_info*)), this, SLOT(newInsuranceCompanyinfo(InsuranceCompany_info*)));

    ui->action->connect(ui->action,SIGNAL(triggered()),this,SLOT(listICs()));


    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    ListOfContractsInfo = DBC.GetListOfContractsInfo();

    foreach (int key, ListOfContractsInfo.keys()) {
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,0,(new QTableWidgetItem(QString::number(ListOfContractsInfo[key]->number))));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,1,(new QTableWidgetItem(QString::number(ListOfContractsInfo[key]->manager_number))));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,2,(new QTableWidgetItem(ListOfContractsInfo[key]->ICi->name)));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,3, new QTableWidgetItem(ListOfContractsInfo[key]->FromDate.toString()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,4, new QTableWidgetItem(ListOfContractsInfo[key]->ToDate.toString()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,5, new QTableWidgetItem(ListOfContractsInfo[key]->info));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,6, new QTableWidgetItem(ListOfContractsInfo[key]->status));
    }
    //не работает на 4.8.6
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
    ui->tableWidget->resizeColumnsToContents();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    addC = new AddContract(DBC.GetListOfInsuranceCompanyinfo(), this);
    connect(addC, SIGNAL(newContractinfo(Contract_info*)), this, SLOT(newContractinfo(Contract_info*)));
    addC->exec();
}


void MainWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{
    viewC = new ContractInfo(*ListOfContractsInfo[ui->tableWidget->item(row,0)->text().toInt()],this);
    connect(viewC, SIGNAL(newcondition(Condition_info*)), this, SLOT(newcondition(Condition_info*)));
    connect(viewC, SIGNAL(TerminateContract(Contract_info*)), this, SLOT(TerminateContract(Contract_info*)));
    viewC->show();
}

void MainWindow::on_pushButton_2_clicked()
{
    this->close();
}

void MainWindow::newContractinfo(Contract_info *Ci){
    Ci->manager_number = manager_number;

    try{
        BC->AddNewContract(Ci);
        ListOfContractsInfo[Ci->number] = Ci;
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,0,(new QTableWidgetItem(QString::number(Ci->number))));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,1,(new QTableWidgetItem(QString::number(Ci->manager_number))));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,2,(new QTableWidgetItem(Ci->ICi->name)));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,3, new QTableWidgetItem(Ci->FromDate.toString()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,4, new QTableWidgetItem(Ci->ToDate.toString()));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,5, new QTableWidgetItem(Ci->info));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,6, new QTableWidgetItem(Ci->status));
        addC->close();
    }catch(...){
        QMessageBox::warning(addC,"Ошибка", "Неверные данные");
    }
}

void MainWindow::newInsuranceCompanyinfo(InsuranceCompany_info *ICi){
    try{
        BC->AddNewInsuranceCompany(ICi);
        addIC->close();
    }catch(...){
        QMessageBox::warning(addIC,"Ошибка", "компания с таким именем уже существует");
    }
}

void MainWindow::listICs(){
    try{
        viewIC = new ViewListOfIC(DBC.GetListOfInsuranceCompanyinfo(),this);
        viewIC->exec();
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Ошибка доступа к базе");
    }
}

void MainWindow::newcondition(Condition_info *Coni){
    Coni->manager_number = manager_number;
    try{
        BC->AddNewCondition(Coni);
//        ListOfContractsInfo[Coni->contract_number]->ArrOfCondition.push_back(Coni);
        ListOfContractsInfo = DBC.GetListOfContractsInfo();
    }catch(...){
        QMessageBox::warning(viewC,"Ошибка", "неверные данные");
    }
}

void MainWindow::TerminateContract(Contract_info *Ci){
    try{
        BC->TerminateContract(Ci->number);
        ListOfContractsInfo[Ci->number]->status = "Рассторгнут";
    }catch(...){
        QMessageBox::warning(viewIC,"Ошибка", "Контракт уже расторгнут");
    }
}
