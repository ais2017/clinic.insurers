#ifndef ADDCONTRACT_H
#define ADDCONTRACT_H

#include <QDialog>
#include "addip.h"

struct helpinfo{
    int contract_number;
};

namespace Ui {
class AddContract;
}

class AddContract : public QDialog
{
    Q_OBJECT

public:
    explicit AddContract(QMap<QString, InsuranceCompany_info*> ListOfInsuranceCompanyinfo, QWidget *parent = 0);
    ~AddContract();

signals:
    void newContractinfo(Contract_info *Ci);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void newInsurancePrograminfo(InsuranceProgram_info *IPi);
private:
    Ui::AddContract *ui;

    QMap<QString, InsuranceCompany_info*> ListOfInsuranceCompanyinfo;

    Contract_info *Ci;
    AddIP *addIP;
};

#endif // ADDCONTRACT_H
