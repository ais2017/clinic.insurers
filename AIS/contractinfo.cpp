#include "contractinfo.h"
#include "ui_contractinfo.h"

ContractInfo::ContractInfo(Contract_info Ci, QWidget *parent) :
    Ci(Ci), QDialog(parent),
    ui(new Ui::ContractInfo)
{
    ui->setupUi(this);
    this->setWindowTitle(this->windowTitle() + QString::number(Ci.number));
    ui->label_2->setText(QString::number(Ci.number));
    ui->label_4->setText(QString::number(Ci.manager_number));
    ui->label_6->setText(Ci.ICi->name);
    ui->label_8->setText(Ci.FromDate.toString());
    ui->label_10->setText(Ci.ToDate.toString());
    ui->label_12->setText(Ci.info);
    ui->label_16->setText(Ci.status);

    for(int ic=0; ic < Ci.ArrOfInsuranceProgram.size(); ic++){
        ui->listWidget->addItem(QString::number(Ci.ArrOfInsuranceProgram[ic]->number));
    }

    for(int con=0; con < Ci.ArrOfCondition.size(); con++){
        ui->listWidget_2->addItem(QString::number(Ci.ArrOfCondition[con]->number));
    }
    if(Ci.status == "Рассторгнут"){
        ui->pushButton_2->setEnabled(false);
        ui->pushButton_3->setEnabled(false);
    }

}

ContractInfo::~ContractInfo()
{
    delete ui;
}

void ContractInfo::on_pushButton_3_clicked()
{
    emit TerminateContract(&Ci);
    ui->label_16->setText("Рассторгнут");
    Ci.status = "Рассторгнут";
    ui->pushButton_2->setEnabled(false);
    ui->pushButton_3->setEnabled(false);
}

void ContractInfo::on_pushButton_clicked()
{
    this->close();
}

void ContractInfo::on_pushButton_2_clicked()
{
    addCon = new AddCondition(Ci);
    connect(addCon, SIGNAL(newConditioninfo(Condition_info*)), this, SLOT(newConditioninfo(Condition_info*)));
    addCon->exec();
}

void ContractInfo::newConditioninfo(Condition_info *Coni){
    try{
        for(int i=0; i< ui->listWidget_2->count(); i++)
            if(ui->listWidget_2->item(i)->text().toInt() == Coni->number)
                throw std::exception();
        ui->listWidget_2->addItem(QString::number(Coni->number));


        for (int i=0; i<Coni->AddListOfInsurancePrograms.size(); i++){
            ui->listWidget->addItem(QString::number(Coni->AddListOfInsurancePrograms[i]->number));
        }
        emit newcondition(Coni);


        addCon->close();
    }catch(...){
        QMessageBox::warning(addCon,"Ошибка", "Дополнительное соглашение\n с таким номером уже существует");
    }
}
