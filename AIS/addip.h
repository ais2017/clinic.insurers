#ifndef ADDIP_H
#define ADDIP_H

#include <QDialog>
#include <QMessageBox>
#include "addservice.h"


namespace Ui {
class AddIP;
}

class AddIP : public QDialog
{
    Q_OBJECT

public:
    explicit AddIP(QWidget *parent = 0);
    ~AddIP();

signals:
    void newInsurancePrograminfo(InsuranceProgram_info *IPi);

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void newServiceinfo(Service_info *Si);

    void on_pushButton_2_clicked();

private:
    Ui::AddIP *ui;

    InsuranceProgram_info *IPi;
    ADDService *addS;
};

#endif // ADDIP_H
