#include "addcontract.h"
#include "ui_addcontract.h"

AddContract::AddContract(QMap<QString, InsuranceCompany_info *> ListOfInsuranceCompanyinfo, QWidget *parent) :
    ListOfInsuranceCompanyinfo(ListOfInsuranceCompanyinfo), QDialog(parent),
    ui(new Ui::AddContract)
{
    ui->setupUi(this);
    ui->lineEdit->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"),this));
    ui->dateEdit->setDate(QDate::currentDate());
    ui->dateEdit_2->setDate(QDate::currentDate());
    foreach (QString key, ListOfInsuranceCompanyinfo.keys()) {
        ui->comboBox->addItem(ListOfInsuranceCompanyinfo[key]->name);
    }

    Ci = new Contract_info;
}

AddContract::~AddContract()
{
    delete ui;
}

void AddContract::on_pushButton_2_clicked()
{
    try{
        if(ui->lineEdit->text() == "" || ui->comboBox->currentText() == "" || ui->dateEdit->date() > ui->dateEdit_2->date())
            throw std::exception();

        Ci->number = ui->lineEdit->text().toInt();
        Ci->ICi = ListOfInsuranceCompanyinfo[ui->comboBox->currentText()];
        Ci->FromDate = ui->dateEdit->date();
        Ci->ToDate = ui->dateEdit_2->date();
        Ci->info = ui->textEdit->toPlainText();
        Ci->status = "Новый";

        emit newContractinfo(Ci);
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Проверьте правильность данных");
    }
}

void AddContract::on_pushButton_3_clicked()
{
    this->close();
}

void AddContract::on_pushButton_clicked()
{
    addIP = new AddIP();
    connect(addIP, SIGNAL(newInsurancePrograminfo(InsuranceProgram_info*)), this, SLOT(newInsurancePrograminfo(InsuranceProgram_info*)));
    addIP->exec();
}

void AddContract::newInsurancePrograminfo(InsuranceProgram_info *IPi){
    try{
        for(int i=0; i< ui->listWidget->count(); i++)
            if(ui->listWidget->item(i)->text().toInt() == IPi->number)
                throw std::exception();

        Ci->ArrOfInsuranceProgram.push_back(IPi);
        ui->listWidget->addItem(QString::number(IPi->number));
        addIP->close();
    }catch(...){
        QMessageBox::warning(addIP,"Ошибка", "Услуга с таким номером уже существует");
    }
}
