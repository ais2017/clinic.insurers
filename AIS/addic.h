#ifndef ADDIC_H
#define ADDIC_H

#include <QDialog>
#include <QMessageBox>
#include "businses_logic/gateway.h"

namespace Ui {
class AddIC;
}

class AddIC : public QDialog
{
    Q_OBJECT

public:
    explicit AddIC(QWidget *parent = 0);
    ~AddIC();

signals:
    void newInsuranceCompanyinfo(InsuranceCompany_info *ICi);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::AddIC *ui;

    InsuranceCompany_info *ICi;
};

#endif // ADDIC_H
