#ifndef ADDSERVICE_H
#define ADDSERVICE_H

#include <QDialog>
#include <QMessageBox>
#include "businses_logic/gateway.h"

namespace Ui {
class ADDService;
}

class ADDService : public QDialog
{
    Q_OBJECT

public:
    explicit ADDService(QWidget *parent = 0);
    ~ADDService();

signals:
    void newServiceinfo(Service_info *Si);

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::ADDService *ui;

    Service_info *Si;
};

#endif // ADDSERVICE_H
