#include "addip.h"
#include "ui_addip.h"

AddIP::AddIP(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddIP)
{
    ui->setupUi(this);

    ui->lineEdit->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"),this));

    IPi = new InsuranceProgram_info;
}

AddIP::~AddIP()
{
    delete ui;
}

void AddIP::on_pushButton_clicked()
{
    addS = new ADDService();
    connect(addS, SIGNAL(newServiceinfo(Service_info*)), this, SLOT(newServiceinfo(Service_info*)));
    addS->exec();
}

void AddIP::on_pushButton_3_clicked()
{
    this->close();
}

void AddIP::newServiceinfo(Service_info *Si){
    try{
        for(int i=0; i< ui->listWidget->count(); i++)
            if(ui->listWidget->item(i)->text().toInt() == Si->number)
                throw std::exception();

        IPi->ArrOfServices.push_back(Si);
        ui->listWidget->addItem(QString::number(Si->number));
        addS->close();
    }catch(...){
        QMessageBox::warning(addS,"Ошибка", "В данной программе\nуслуга с таким номером уже существует");
    }
}

void AddIP::on_pushButton_2_clicked()
{
    try{
        if(ui->lineEdit->text() == "")
            throw std::exception();

        IPi->number = ui->lineEdit->text().toInt();
        IPi->info = ui->textEdit->toPlainText();
        emit newInsurancePrograminfo(IPi);
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Проверьте правильность данных");
    }
}
