#include "viewlistofic.h"
#include "ui_viewlistofic.h"

ViewListOfIC::ViewListOfIC(QMap<QString, InsuranceCompany_info *> ListOfInsuranceCompanyinfo, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ViewListOfIC)
{
    ui->setupUi(this);
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);

    foreach (QString key, ListOfInsuranceCompanyinfo.keys()) {
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,0,(new QTableWidgetItem(ListOfInsuranceCompanyinfo[key]->name)));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,1,(new QTableWidgetItem(ListOfInsuranceCompanyinfo[key]->address)));
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1,2,(new QTableWidgetItem(ListOfInsuranceCompanyinfo[key]->phonenumber)));
    }

    //не работает на 4.8.6
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableWidget->resizeColumnsToContents();
}

ViewListOfIC::~ViewListOfIC()
{
    delete ui;
}

void ViewListOfIC::on_pushButton_clicked()
{
    this->close();
}
