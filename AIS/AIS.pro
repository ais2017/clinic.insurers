#-------------------------------------------------
#
# Project created by QtCreator 2018-12-11T17:07:20
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AIS
TEMPLATE = app

#DEFINES += -O0

SOURCES += main.cpp\
        mainwindow.cpp \
    addcontract.cpp \
    addip.cpp \
    addservice.cpp \
    contractinfo.cpp \
    addcondition.cpp \
    addic.cpp \
    businses_logic/business_component.cpp \
    businses_logic/class_clinic_insurers.cpp \
    businses_logic/gateway.cpp \
    viewlistofic.cpp

HEADERS  += mainwindow.h \
    addcontract.h \
    addip.h \
    addservice.h \
    contractinfo.h \
    addcondition.h \
    addic.h \
    businses_logic/business_component.h \
    businses_logic/class_clinic_insurers.h \
    businses_logic/gateway.h \
    viewlistofic.h

FORMS    += mainwindow.ui \
    addcontract.ui \
    addip.ui \
    addservice.ui \
    contractinfo.ui \
    addcondition.ui \
    addic.ui \
    viewlistofic.ui

INCLUDEPATH += /usr/local/include

LIBS    += -lmongoc-1.0 \
    -lbson-1.0

INCLUDEPATH += /usr/local/lib
LIBS += -lqjson-qt5


