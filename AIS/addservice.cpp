#include "addservice.h"
#include "ui_addservice.h"

ADDService::ADDService(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ADDService)
{
    ui->setupUi(this);
    ui->lineEdit->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"),this));
    ui->lineEdit_3->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"),this));
}

ADDService::~ADDService()
{
    delete ui;
}

void ADDService::on_pushButton_2_clicked()
{
    this->close();
}

void ADDService::on_pushButton_clicked()
{
    try{
    if(ui->lineEdit->text() == "" || ui->lineEdit_2->text() == "" || ui->lineEdit_3->text() == "")
        throw std::exception();
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Проверьте правильность данных");
    }

    Si = new Service_info;
    Si->number = ui->lineEdit->text().toInt();
    Si->name = ui->lineEdit_2->text();
    Si->info = ui->textEdit->toPlainText();
    Si->cost = ui->lineEdit_3->text().toInt();
    emit newServiceinfo(Si);
}
