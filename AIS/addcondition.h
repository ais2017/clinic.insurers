#ifndef ADDCONDITION_H
#define ADDCONDITION_H

#include <QDialog>
#include <QDate>
#include "businses_logic/gateway.h"
#include "addip.h"

namespace Ui {
class AddCondition;
}

class AddCondition : public QDialog
{
    Q_OBJECT

public:
    explicit AddCondition(Contract_info Ci, QWidget *parent = 0);
    ~AddCondition();

signals:
    void newConditioninfo(Condition_info *Coni);

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_4_clicked();

    void newInsurancePrograminfo(InsuranceProgram_info *IPi);
    void on_pushButton_2_clicked();

private:
    Ui::AddCondition *ui;

    Contract_info Ci;
    Condition_info *Coni;

    AddIP *addIP;
};

#endif // ADDCONDITION_H
