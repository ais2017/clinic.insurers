#ifndef CLASS_CLINIC_INSURERS_H
#define CLASS_CLINIC_INSURERS_H

#include <QString>
#include <QDate>
#include <QVector>
#include <QMap>
#include <QDebug>


class Policy
{
public:
    Policy(QString PNunber, QString PPatient, QDate PTillDate, int InsuranceProgramNumber);
    bool isValid();
    QString GetNumber();
    QString GetPatient();
    QDate GetTillDate();
    int GetInsuranceProgramNumber();
private:
    QString number;
    QString patient;
    QDate TillDate;
    int IPNumber;

};



class InsuranceCompany
{
public:
    InsuranceCompany(QString ICName, QString ICAddress, QString ICPhonenum);
    void UpdateAddress(QString newAddress);
    void UpdatePhonenumber(QString newPhonenumber);
    QString GetName();
    QString GetAddress();
    QString GetPhonenumber();
private:
    QString name;
    QString address;
    QString phonenum;
};



class Service
{
public:
    Service(int SNumber, QString SName, QString SInfo, int SCost);
    int GetNumber();
    QString GetName();
    QString GetInfo();
    int GetCost();
private:
    int number;
    QString name;
    QString info;
    int cost;
};



class Emploee
{
public:
    Emploee(int ENumber, QString EPosition, QString EFIO);
    int GetNumber();
    QString GetPosition();
    QString GetFIO();
private:
    int number;
    QString position;
    QString FIO;
};



class Patient
{
public:
    Patient(int PNumber, QString PFIO, QString PPassport);
    void AddPolicy(Policy *P_policy);
    Policy *GetPolicy(QString PolicyNumber);
    QMap<QString, Policy *> &GetPolicys();
    int GetNumber();
    QString GetFIO();
    QString GetPassport();
private:
    int number;
    QMap<QString, Policy*> ListOfPolicys;
    QString FIO;
    QString passport;
};



class InsuranceProgram
{
public:
    InsuranceProgram(int ICNumber, QString ICInfo);
    void IssuePolicy(Policy *newPolicy);
    void AddService(Service *newService);
    void RemoveService(int SNumber);
    Service *GetService(int SNumber);
    QMap<int, Service*> &GetServices();
    Policy *GetPolicy(QString PNumber);
    QMap<QString, Policy*> &GetPolicys();
    int GetNumber();
    QString GetInfo();
private:
    int number;
    QMap<int, Service*> ListOfServices;
    QMap<QString, Policy*> ListOfPolicys;
    QString info;
};




class Condition
{
public:
    Condition(int CNumber, int ContractNumber, QString CInfo, Emploee* manager, QDate ToDate);
//    void SetNewManager(Emploee *Manager);
//    void SetNewToDate(QDate d);
//    void SetNewInfo(QString newInfo);
    void AddToAddListOfInsuranceProgram(InsuranceProgram *newInsuranceProgram);
    void RemoveFromAddListOfInsuranceProgram(int IPNumber);
    void AddToDellListOfInsuranceProgram(InsuranceProgram *newInsuranceProgram);
    void RemoveFromDellListOfInsuranceProgram(int IPNumber);
    int GetNumber();
    int GetContractNumber();
    Emploee *GetManager();
    QString GetInfo();
    QDate GetToDate();
    QMap<int, InsuranceProgram *> &GetAddListOfInsurancePrograms();
    QMap<int, InsuranceProgram *> &GetDellListOfInsurancePrograms();
private:
    int number;
    int ContractNumber;
    Emploee *newManager;
    QDate newToDate;
    QString newInfo;
    QMap<int, InsuranceProgram*> AddListOfInsurancePrograms;
    QMap<int, InsuranceProgram*> DellListOfInsurancePrograms;
};




class Contract
{
public:
    Contract(int CNumber, Emploee *CManager, InsuranceCompany *CICName, QDate CFromDate, QDate CToDate, QString CInfo);
    void AddInsuranceProgram(InsuranceProgram *newInsuranceProgram);
    void RemoveInsuranceProgram(int IPNumber);
    void TerminateContract();
    void SetToDate(QDate newToDate);
    void SetInfo(QString newInfo);
    void SetEmploee(Emploee *newEmploee);
    void Approve();
    void AddCondition(Condition *newCondition);             //добавить обновление контракта
    InsuranceProgram *GetInsuranceProgram(int IPNumber);
    QMap<int, InsuranceProgram *> &GetInsurancePrograms();
    Condition *GetCondition(int ConditionNumber);
    QMap<int, Condition *> &GetConditions();
    int GetNumber();
    Emploee *GetManager();
    InsuranceCompany *GetInsuranceCompany();
    QDate GetFromDate();
    QDate GetToDate();
    QString GetInfo();
    QString GetStatus();
private:
    int number;
    Emploee *manager;
    InsuranceCompany *IC;
    QDate FromDate;
    QDate ToDate;
    QString info;
    QMap<int, InsuranceProgram*> ListOfInsurancePrograms;
    QMap<int, Condition*> ListOfConditions;
    QString status;
};


#endif // CLASS_CLINIC_INSURERS_H
