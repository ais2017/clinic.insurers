#include "class_clinic_insurers.h"

//Policy
Policy::Policy(QString PNunber, QString PPatient, QDate PTillDate, int InsuranceProgramNumber)
    : number(PNunber), patient(PPatient), TillDate(PTillDate), IPNumber(InsuranceProgramNumber)
{}

bool Policy::isValid(){
    if (TillDate > QDate::currentDate())
        return true;
    return false;
}

QString Policy::GetNumber(){
    return number;
}

QString Policy::GetPatient(){
    return patient;
}

QDate Policy::GetTillDate(){
    return TillDate;
}

int Policy::GetInsuranceProgramNumber(){
    return IPNumber;
}



//InsuranceCompany
InsuranceCompany::InsuranceCompany(QString ICName, QString ICAddress, QString ICPhonenum)
    : name(ICName), address(ICAddress), phonenum(ICPhonenum)
{}

void InsuranceCompany::UpdateAddress(QString newAddress){
    address = newAddress;
    return;
}

void InsuranceCompany::UpdatePhonenumber(QString newPhonenumber){
    phonenum = newPhonenumber;
    return;
}

QString InsuranceCompany::GetName(){
    return name;
}

QString InsuranceCompany::GetAddress(){
    return address;
}

QString InsuranceCompany::GetPhonenumber(){
    return phonenum;
}



//Service
Service::Service(int SNumber, QString SName, QString SInfo, int SCost)
    : number(SNumber), name(SName), info(SInfo), cost(SCost)
{}

int Service::GetNumber(){
    return number;
}

QString Service::GetName(){
    return name;
}

QString Service::GetInfo(){
    return info;
}

int Service::GetCost(){
   return cost;
}



//Emploee
Emploee::Emploee(int ENumber, QString EPosition, QString EFIO)
    : number(ENumber), position(EPosition), FIO(EFIO)
{}

int Emploee::GetNumber(){
    return number;
}

QString Emploee::GetPosition(){
    return position;
}

QString Emploee::GetFIO(){
    return FIO;
}



//Patient
Patient::Patient(int PNumber, QString PFIO, QString PPassport)
    : number(PNumber), FIO(PFIO), passport(PPassport)
{}

void Patient::AddPolicy(Policy *P_policy){
    if (P_policy){
        ListOfPolicys[P_policy->GetNumber()] = P_policy;
    } else
        throw std::exception();
}

 QMap<QString, Policy *> &Patient::GetPolicys(){
    return ListOfPolicys;
}

Policy *Patient::GetPolicy(QString PolicyNumber){
    if(ListOfPolicys.contains(PolicyNumber))
            return ListOfPolicys[PolicyNumber];

    throw std::exception();
    return NULL;
}

int Patient::GetNumber(){
    return number;
}

QString Patient::GetFIO(){
    return FIO;
}

QString Patient::GetPassport(){
    return passport;
}



//InsuranceProgram
InsuranceProgram::InsuranceProgram(int ICNumber, QString ICInfo)
    : number(ICNumber), info(ICInfo)
{}

void InsuranceProgram::IssuePolicy(Policy *newPolicy){
    if (newPolicy){
        ListOfPolicys[newPolicy->GetNumber()] = newPolicy;
        return;
    }

    throw std::exception();
}

QMap<int, Service *> &InsuranceProgram::GetServices(){
    return ListOfServices;
}

QMap<QString, Policy *> &InsuranceProgram::GetPolicys(){
    return ListOfPolicys;
}

void InsuranceProgram::AddService(Service *newService){
    if (newService){
        ListOfServices[newService->GetNumber()] = newService;
        return;
    }

    throw std::exception();
}

void InsuranceProgram::RemoveService(int SNumber){
    if(ListOfServices.contains(SNumber))
        ListOfServices.remove(SNumber);
    else
        throw std::exception();
}

Service *InsuranceProgram::GetService(int SNumber){
    if(ListOfServices.contains(SNumber))
        return ListOfServices[SNumber];

    throw std::exception();
    return NULL;
}

Policy *InsuranceProgram::GetPolicy(QString PNumber){
    if(ListOfPolicys.contains(PNumber))
        return ListOfPolicys[PNumber];

    throw std::exception();
    return NULL;
}

int InsuranceProgram::GetNumber(){
    return number;
}

QString InsuranceProgram::GetInfo(){
    return info;
}



//Condition
Condition::Condition(int CNumber, int ContractNumber, QString CInfo, Emploee *manager, QDate ToDate)
    : number(CNumber), ContractNumber(ContractNumber), newInfo(CInfo), newManager(manager), newToDate(ToDate)
{}

//void Condition::SetNewManager(Emploee *manager){
//    newManager = manager;
//}

//void Condition::SetNewToDate(QDate d){
//    newToDate = d;
//}

//void Condition::SetNewInfo(QString newInfo){
//    this->newInfo = newInfo;
//}

void Condition::AddToAddListOfInsuranceProgram(InsuranceProgram *newInsuranceProgram){
    if (newInsuranceProgram){
        AddListOfInsurancePrograms[newInsuranceProgram->GetNumber()] = newInsuranceProgram;
        return;
    }
    throw std::exception();
}

void Condition::RemoveFromAddListOfInsuranceProgram(int IPNumber){
    if(AddListOfInsurancePrograms.contains(IPNumber))
        AddListOfInsurancePrograms.remove(IPNumber);
    else
        throw std::exception();
}

void Condition::AddToDellListOfInsuranceProgram(InsuranceProgram *newInsuranceProgram){
    if (newInsuranceProgram){
        DellListOfInsurancePrograms[newInsuranceProgram->GetNumber()] = newInsuranceProgram;
        return;
    }
    throw std::exception();
}

void Condition::RemoveFromDellListOfInsuranceProgram(int IPNumber){
    if(DellListOfInsurancePrograms.contains(IPNumber))
        DellListOfInsurancePrograms.remove(IPNumber);
    else
        throw std::exception();
}

int Condition::GetNumber(){
    return number;
}

int Condition::GetContractNumber(){
    return ContractNumber;
}

Emploee *Condition::GetManager(){
    return newManager;
}

QString Condition::GetInfo(){
    return newInfo;
}

QDate Condition::GetToDate(){
    return newToDate;
}

QMap<int, InsuranceProgram *> &Condition::GetAddListOfInsurancePrograms(){
    return AddListOfInsurancePrograms;
}

QMap<int, InsuranceProgram *> &Condition::GetDellListOfInsurancePrograms(){
    return DellListOfInsurancePrograms;
}





//Contract
Contract::Contract(int CNumber, Emploee *CManager, InsuranceCompany *CICName, QDate CFromDate, QDate CToDate, QString CInfo)
    : number(CNumber), manager(CManager), IC(CICName), FromDate(CFromDate), ToDate(CToDate), info(CInfo), status("Новый")
{
    if (CFromDate > CToDate)
        throw std::exception();

}

void Contract::AddCondition(Condition *newCondition){
    if (newCondition){
        ListOfConditions[newCondition->GetNumber()] = newCondition;

        //добавить обновление контракта
        return;
    }
    throw std::exception();
}

void Contract::AddInsuranceProgram(InsuranceProgram *newInsuranceProgram){
    if (newInsuranceProgram){
        ListOfInsurancePrograms[newInsuranceProgram->GetNumber()] = newInsuranceProgram;
        return;
    }
    throw std::exception();
}

void Contract::RemoveInsuranceProgram(int IPNumber){
    if(ListOfInsurancePrograms.contains(IPNumber))
        ListOfInsurancePrograms.remove(IPNumber);
    else
        throw std::exception();
}

void Contract::TerminateContract(){
    status = "Рассторгнут";
}

void Contract::SetToDate(QDate newToDate){
    if(newToDate > FromDate)
        ToDate = newToDate;
    else
       throw std::exception();
}

void Contract::SetInfo(QString newInfo){
    info = newInfo;
}

void Contract::SetEmploee(Emploee *newEmploee){
    if(newEmploee)
        manager = newEmploee;
    else
        throw std::exception();
}

QMap<int, InsuranceProgram *> &Contract::GetInsurancePrograms(){
    return ListOfInsurancePrograms;
}

InsuranceProgram *Contract::GetInsuranceProgram(int IPNumber){
    if(ListOfInsurancePrograms.contains(IPNumber))
        return ListOfInsurancePrograms[IPNumber];

    throw std::exception();
    return NULL;
}

void Contract::Approve(){
    if (status == "Рассторгнут")
        return;

    if (ToDate < QDate::currentDate()){
        status = "Истекший";
        return;
    }

    if ((ListOfConditions.size() == 0) || (ListOfInsurancePrograms.size() == 0) ){
        status = "Новый";
    } else {
        status = "Активный";
    }
}

Condition *Contract::GetCondition(int ConditionNumber){
    if(ListOfConditions.contains(ConditionNumber))
        return ListOfConditions[ConditionNumber];

    throw std::exception();
    return NULL;
}

QMap<int, Condition *> &Contract::GetConditions(){
    return ListOfConditions;
}

int Contract::GetNumber(){
    return number;
}

Emploee *Contract::GetManager(){
    return manager;
}

InsuranceCompany *Contract::GetInsuranceCompany(){
    return IC;
}

QDate Contract::GetFromDate(){
    return FromDate;
}

QDate Contract::GetToDate(){
    return ToDate;
}

QString Contract::GetInfo(){
    return info;
}

QString Contract::GetStatus(){
    return status;
}
