#include "gateway.h"


Clinic_Patient_Component::Clinic_Patient_Component(){

}

Patient *Clinic_Patient_Component::GetPatient(int number){
    if (number == 400){
        QDate TillDate(2018,12,01);
        Policy *Pl_400_1 = new Policy("99900140002", "F400 I400 O400", TillDate, 999001);
        Policy *Pl_400_2 = new Policy("88800140001", "F400 I400 O400", QDate::currentDate().addYears(1), 888001);

        Patient *P_400 = new Patient(400, "F400 I400 O400", "66 66 098245");
        P_400->AddPolicy(Pl_400_1);
        P_400->AddPolicy(Pl_400_2);

        return P_400;
    }

    throw std::exception();
}




Clinic_Personal_Component::Clinic_Personal_Component(){

}


Emploee *Clinic_Personal_Component::GetManager(int number){
    Emploee *manager = new Emploee(number, "Manager FIO", "Manager");
    return manager;
}






DataBase_Component::DataBase_Component::DataBase_Component(){
    /* get a handle to our collection */
    client = mongoc_client_new ("mongodb://localhost:27017");
    Company_collection = mongoc_client_get_collection (client, "Clinic_insurers", "Company");
    Contract_collection = mongoc_client_get_collection (client, "Clinic_insurers", "Contract");
}

void DataBase_Component::SaveContract(Contract *contract){
    QMap<int, InsuranceProgram*> ListOfInsurancePrograms = contract->GetInsurancePrograms();
    QMap<int, InsuranceProgram*>::iterator ip_it = ListOfInsurancePrograms.begin();

    QMap<int, Service*> ListOfServices;
    QMap<int, Service*>::iterator s_it;

    QMap<QString, Policy*> ListOfPolicys;
    QMap<QString, Policy*>::iterator p_it;

    bson_error_t error;
    bson_oid_t oid;
    uint32_t i,j,k;
    char buf[16];
    const char *key;
    size_t keylen;
    bson_t query;
    bson_t *doc;
    bson_t ips;
    bson_t ip;
    bson_t s;
    bson_t ss;
    bson_t p;
    bson_t ps;
    bson_t cs;

    /* insert a document */
    doc = bson_new();
    bson_oid_init (&oid, NULL);
    BSON_APPEND_OID (doc, "_id", &oid);
    BSON_APPEND_INT64 (doc, "Number", contract->GetNumber());
    BSON_APPEND_INT64 (doc, "Manager number", contract->GetManager()->GetNumber());
    BSON_APPEND_UTF8 (doc, "Insurers company", contract->GetInsuranceCompany()->GetName().toUtf8());
    BSON_APPEND_UTF8 (doc, "From date", contract->GetFromDate().toString("dd.MM.yyyy").toUtf8());
    BSON_APPEND_UTF8 (doc, "To date", contract->GetToDate().toString("dd.MM.yyyy").toUtf8());
    BSON_APPEND_UTF8 (doc, "Info", contract->GetInfo().toUtf8());
    BSON_APPEND_UTF8 (doc, "Status", contract->GetStatus().toUtf8());

        //страховые программы
        BSON_APPEND_ARRAY_BEGIN(doc,"Insurers programs", &ips);
        for(i=0;ip_it != ListOfInsurancePrograms.end(); ++ip_it, i++){
            keylen = bson_uint32_to_string(i,&key,buf,sizeof buf);
            bson_append_document_begin(&ips,key,(int) keylen,&ip);
            BSON_APPEND_INT64(&ip,"Number", ip_it.value()->GetNumber());
                //услуги
                BSON_APPEND_ARRAY_BEGIN(&ip,"Services", &ss);
                ListOfServices = ip_it.value()->GetServices();
                s_it = ListOfServices.begin();
                for(j=0;s_it != ListOfServices.end(); ++s_it, j++){
                    keylen = bson_uint32_to_string(j,&key,buf,sizeof buf);
                    bson_append_document_begin(&ss,key,(int) keylen,&s);
                    BSON_APPEND_INT64(&s,"Number", s_it.value()->GetNumber());
                    BSON_APPEND_UTF8(&s, "Name", s_it.value()->GetName().toUtf8());
                    BSON_APPEND_UTF8(&s, "Info", s_it.value()->GetInfo().toUtf8());
                    BSON_APPEND_INT64(&s,"Cost", s_it.value()->GetCost());
                    bson_append_document_end(&ss,&s);
                }
                bson_append_array_end(&ip,&ss);

                //полиса
                BSON_APPEND_ARRAY_BEGIN(&ip,"Policys", &ps);
                ListOfPolicys = ip_it.value()->GetPolicys();
                p_it = ListOfPolicys.begin();
                for(k=0;p_it != ListOfPolicys.end(); ++p_it, k++){
                    keylen = bson_uint32_to_string(k,&key,buf,sizeof buf);
                    bson_append_document_begin(&ps,key,(int) keylen,&p);
                    BSON_APPEND_UTF8(&p,"Number", p_it.value()->GetNumber().toUtf8());
                    BSON_APPEND_UTF8(&p, "Patient", p_it.value()->GetPatient().toUtf8());
                    BSON_APPEND_UTF8(&p, "Till date", p_it.value()->GetTillDate().toString("dd.MM.yyyy").toUtf8());
                    BSON_APPEND_INT64(&p,"IC number", p_it.value()->GetInsuranceProgramNumber());
                    bson_append_document_end(&ps,&p);
                }
                bson_append_array_end(&ip,&ps);

            bson_append_document_end(&ips,&ip);
        }
        bson_append_array_end(doc,&ips);

        //доп соглашения
        BSON_APPEND_ARRAY_BEGIN(doc,"Conditions", &cs);

        bson_append_array_end(doc,&cs);

    mongoc_collection_insert (Contract_collection, MONGOC_INSERT_NONE, doc, NULL, &error);

    /* build a query to execute */
    bson_init (&query);
    BSON_APPEND_OID (&query, "_id", &oid);

    bson_destroy (&query);
    bson_destroy (doc);
}

void DataBase_Component::UpdateContract(Contract *contract){
    mongoc_cursor_t *cursor;
    bson_error_t error;
    bson_t *query;
    bson_t *update;

    query = bson_new();
    BSON_APPEND_INT64(query, "Number", contract->GetNumber());
    cursor = mongoc_collection_find(Contract_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    update = BCON_NEW ("$set", "{",
                       "Status", BCON_UTF8(contract->GetStatus().toUtf8()),
                       "}");

    mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);

    bson_destroy (query);
    bson_destroy (update);
}

void DataBase_Component::UpdateContract(Contract *contract, Condition *condition){
    QMap<int, InsuranceProgram*> ListOfInsurancePrograms;
    QMap<int, InsuranceProgram*>::iterator ip_it;

    QMap<int, Service*> ListOfServices;
    QMap<int, Service*>::iterator s_it;

    QMap<QString, Policy*> ListOfPolicys;
    QMap<QString, Policy*>::iterator p_it;

    //написать логику обновления
    mongoc_cursor_t *cursor;
    bson_error_t error;
    bson_t *query;
    bson_t *update;

    query = bson_new();
    BSON_APPEND_INT64(query, "Number", contract->GetNumber());
    cursor = mongoc_collection_find(Contract_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    update = BCON_NEW ("$set", "{",
                       "Manager number", BCON_INT64(condition->GetManager()->GetNumber()),
                       "Info", BCON_UTF8(condition->GetInfo().toUtf8()),
                       "To date", BCON_UTF8(condition->GetToDate().toString("dd.MM.yyyy").toUtf8()),
                       "Status", BCON_UTF8(contract->GetStatus().toUtf8()),
                       "}", "$push", "{",
                       "Conditions",  "{", "Number", BCON_INT64(condition->GetNumber()),
                                          "Contract number", BCON_INT64(condition->GetContractNumber()),
                                          "Manager number", BCON_INT64(condition->GetManager()->GetNumber()),
                                          "To date", BCON_UTF8(contract->GetToDate().toString("dd.MM.yyyy").toUtf8()),
                                          "Info", BCON_UTF8(condition->GetInfo().toUtf8()),
                                          "New insurers programs", "[", "]",
                                          "Dell insurers programs", "[", "]",
                                     "}",
                                 "}");

    mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);


    ListOfInsurancePrograms = condition->GetAddListOfInsurancePrograms();
    ip_it = ListOfInsurancePrograms.begin();
    for(int i=0;ip_it != ListOfInsurancePrograms.end(); ++ip_it, i++){
        update = BCON_NEW ("$push", "{",
                               "Insurers programs", "{",
                                   "Number", BCON_INT64(ip_it.value()->GetNumber()),
                                   "Info", BCON_UTF8(ip_it.value()->GetInfo().toUtf8()),
                                   "Services", "[", "]",
                                   "Policys", "[", "]",
                               "}",

                               "Conditions.0.New insurers programs", "{",
                                       "Number", BCON_INT64(ip_it.value()->GetNumber()),
                                       "Info", BCON_UTF8(ip_it.value()->GetInfo().toUtf8()),
                                       "Services", "[", "]",
                                       "Policys", "[", "]",
                                   "}",
                           "}");

        mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);

        ListOfServices = ip_it.value()->GetServices();
        s_it = ListOfServices.begin();
        for(int s=0; s_it != ListOfServices.end(); ++s_it, s++){
            update = BCON_NEW ("$push", "{",
                                    "Conditions.0.New insurers programs.1.Services", "{",
                                       "Number", BCON_INT64(s_it.value()->GetNumber()),
                                       "Name", BCON_UTF8(s_it.value()->GetName().toUtf8()),
                                       "Info", BCON_UTF8(s_it.value()->GetInfo().toUtf8()),
                                       "Cost", BCON_INT64(s_it.value()->GetCost()),
                                    "}",

                                    "Insurers programs.1.Services", "{",
                                       "Number", BCON_INT64(s_it.value()->GetNumber()),
                                       "Name", BCON_UTF8(s_it.value()->GetName().toUtf8()),
                                       "Info", BCON_UTF8(s_it.value()->GetInfo().toUtf8()),
                                       "Cost", BCON_INT64(s_it.value()->GetCost()),
                                   "}",
                               "}");

            mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);

        }

        for(int s=0; s_it != ListOfServices.end(); ++s_it, s++){
            update = BCON_NEW ("$push", "{",
                                    "Conditions.0.New insurers programs.0.Policys", "{",
                                        "Number", BCON_UTF8(p_it.value()->GetNumber().toUtf8()),
                                        "Patient", BCON_UTF8(p_it.value()->GetPatient().toUtf8()),
                                        "Till date", BCON_UTF8(p_it.value()->GetTillDate().toString("dd.MM.yyyy").toUtf8()),
                                        "IC number", BCON_INT64(p_it.value()->GetInsuranceProgramNumber()),
                                    "}",

                                   "Insurers programs.1.Policys", "{",
                                       "Number", BCON_UTF8(p_it.value()->GetNumber().toUtf8()),
                                       "Patient", BCON_UTF8(p_it.value()->GetPatient().toUtf8()),
                                       "Till date", BCON_UTF8(p_it.value()->GetTillDate().toString("dd.MM.yyyy").toUtf8()),
                                       "IC number", BCON_INT64(p_it.value()->GetInsuranceProgramNumber()),
                                   "}",
                               "}");
            mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);
        }
    }

    //цикл по удаленным программам
    ListOfInsurancePrograms = condition->GetDellListOfInsurancePrograms();
    ip_it = ListOfInsurancePrograms.begin();
    for(int d=0; ip_it != ListOfInsurancePrograms.end(); ++ip_it, d++){
        update = BCON_NEW ("$push", "{",
                               "Conditions.0.Dell insurers programs", "{",
                                   "Number", BCON_INT64(ip_it.value()->GetNumber()),
                                   "Info", BCON_UTF8(ip_it.value()->GetInfo().toUtf8()),
                                   "Services", "[", "]",
                                   "Policys", "[", "]",
                               "}",

                           "}",
                             "$pull", "{",
                                "Insurers programs", "{", "Number", BCON_INT64(ip_it.value()->GetNumber()), "}",
                                "}");

        mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);

        ListOfServices = ip_it.value()->GetServices();
        s_it = ListOfServices.begin();
        for(int s=0; s_it != ListOfServices.end(); ++s_it, s++){
            update = BCON_NEW ("$push", "{",
                                    "Conditions.0.Dell insurers programs.0.Services", "{",
                                       "Number", BCON_INT64(s_it.value()->GetNumber()),
                                       "Name", BCON_UTF8(s_it.value()->GetName().toUtf8()),
                                       "Info", BCON_UTF8(s_it.value()->GetInfo().toUtf8()),
                                       "Cost", BCON_INT64(s_it.value()->GetCost()),
                                    "}",

                               "}");

            mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);

        }

        for(int s=0; s_it != ListOfServices.end(); ++s_it, s++){
            update = BCON_NEW ("$push", "{",
                                    "Conditions.0.Dell insurers programs.0.Policys", "{",
                                        "Number", BCON_UTF8(p_it.value()->GetNumber().toUtf8()),
                                        "Patient", BCON_UTF8(p_it.value()->GetPatient().toUtf8()),
                                        "Till date", BCON_UTF8(p_it.value()->GetTillDate().toString("dd.MM.yyyy").toUtf8()),
                                        "IC number", BCON_INT64(p_it.value()->GetInsuranceProgramNumber()),
                                    "}",
                               "}");
            mongoc_collection_update(Contract_collection, MONGOC_UPDATE_NONE, query, update, NULL, &error);
        }
    }

    mongoc_cursor_destroy(cursor);
    bson_destroy (query);
    bson_destroy (update);
}

void DataBase_Component::SaveInsuranceCompany(InsuranceCompany* insurancecompany){

    bson_error_t error;
    bson_oid_t oid;
    bson_t query;
    bson_t doc;

    /* insert a document */
    bson_init (&doc);
    bson_oid_init (&oid, NULL);
    BSON_APPEND_OID (&doc, "_id", &oid);
    BSON_APPEND_UTF8 (&doc, "Name", insurancecompany->GetName().toUtf8());
    BSON_APPEND_UTF8 (&doc, "Address", insurancecompany->GetAddress().toUtf8());
    BSON_APPEND_UTF8 (&doc, "Phonenumber", insurancecompany->GetPhonenumber().toUtf8());
    mongoc_collection_insert (Company_collection, MONGOC_INSERT_NONE, &doc, NULL, &error);

        /* build a query to execute */
        bson_init (&query);
        BSON_APPEND_OID (&query, "_id", &oid);

//    mongoc_collection_destroy (collection);
//    mongoc_client_destroy (client);
    bson_destroy (&query);
    bson_destroy (&doc);
}

QMap<int, Contract_info*> DataBase_Component::GetListOfContractsInfo(){
    QMap<int, Contract_info*> ListOfContractsInfo;
    Contract_info *contract;
    InsuranceProgram_info *ipi;
    Service_info *si;
    Policy_info *pi;
    Emploee *manager;
    InsuranceCompany *company;
    Condition_info *coni;

    QDate FromDate, ToDate;

    QJson::Parser parser;
    QVariantMap result;
    bool ok;

    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;

    query = bson_new ();
    cursor = mongoc_collection_find(Contract_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    //цикл по контрактам
    while (mongoc_cursor_next (cursor, &doc)) {
        result = parser.parse(bson_as_json (doc, NULL), &ok).toMap();
        Clinic_Personal_Component CPrC;
        manager = CPrC.GetManager(result["Manager number"].toInt());
        company = this->GetInsuranceCompany(result["Insurers company"].toString());
        contract = new Contract_info;
        contract->number = result["Number"].toInt();
        contract->manager_number = result["Manager number"].toInt();
        contract->ICi = new InsuranceCompany_info;
        contract->ICi->name = company->GetName();
        contract->ICi->address = company->GetAddress();
        contract->ICi->phonenumber = company->GetPhonenumber();
        FromDate = QDate::fromString(result["From date"].toString(), "dd.MM.yyyy");
        contract->FromDate = FromDate;
        ToDate = QDate::fromString(result["To date"].toString(), "dd.MM.yyyy");
        contract->ToDate = ToDate;
        contract->info = result["Info"].toString();
        contract->status = result["Status"].toString();

        //страховые программы
        foreach (QVariant listofip, result["Insurers programs"].toList()){
            QVariantMap mapip = listofip.toMap();

            ipi = new InsuranceProgram_info;
            ipi->number = mapip["Number"].toInt();
            ipi->info = mapip["Info"].toString();

            //услуги страховой программы
            foreach (QVariant listofservices, result["Services"].toList()){
                QVariantMap mapservices = listofservices.toMap();

                si = new Service_info;
                si->number = result["Number"].toInt();
                si->name = result["Name"].toString();
                si->cost = result["Cost"].toInt();
                si->info = result["Info"].toString();
                ipi->ArrOfServices.push_back(si);
            }

            //полисы страховой программы
            foreach(QVariant listofpolicys, result["Policys"].toList()){
                QVariantMap mappolisys = listofpolicys.toMap();

                pi = new Policy_info;
                pi->number = result["Number"].toString();
                pi->patient = result["Patient"].toString();
                pi->IC_program = result["IC number"].toInt();
                pi->TillDate = QDate::fromString(result["Number"].toString(),"dd.MM.yyyy");
                ipi->ArrOfPolicys.push_front(pi);
            }
            contract->ArrOfInsuranceProgram.push_back(ipi);
        }

        //дополнительные соглашанеия
        foreach (QVariant listofcondition, result["Conditions"].toList()){
            QVariantMap mapip = listofcondition.toMap();

            coni = new Condition_info;
            coni->number = result["Number"].toInt();
            coni->contract_number = result["Contract number"].toInt();
            coni->manager_number = result["Manager number"].toInt();
            coni->info = result["Info"].toString();
            coni->ToDate = QDate::fromString(result["To date"].toString(), "dd,MM,yyyy");

            //новые страховые программы
            foreach (QVariant listofaddip, result["Add insurers programs"].toList()){
                QVariantMap mapaddip = listofaddip.toMap();

                ipi = new InsuranceProgram_info;
                ipi->number = mapaddip["Number"].toInt();
                ipi->info = mapaddip["Info"].toString();

                //услуги страховой программы
                foreach (QVariant listofservices, result["Services"].toList()){
                    QVariantMap mapservices = listofservices.toMap();

                    si = new Service_info;
                    si->number = result["Number"].toInt();
                    si->name = result["Name"].toString();
                    si->cost = result["Cost"].toInt();
                    si->info = result["Info"].toString();

                    ipi->ArrOfServices.push_back(si);
                }

                //полюсы страховой программы
                foreach (QVariant listofpolicys, result["Policys"].toList()){
                    QVariantMap mappolisys = listofpolicys.toMap();

                    pi = new Policy_info;
                    pi->number = result["Number"].toString();
                    pi->patient = result["Patient"].toString();
                    pi->IC_program = result["IC number"].toInt();
                    pi->TillDate = QDate::fromString(result["Number"].toString(),"dd.MM.yyyy");

                    ipi->ArrOfPolicys.push_front(pi);
                }

                coni->AddListOfInsurancePrograms.push_back(ipi);
            }

            //удаляемые страховые программы
            foreach (QVariant listofdellip, result["Dell insurers programs"].toList()){
                QVariantMap mapaddip = listofdellip.toMap();

                ipi = new InsuranceProgram_info;
                ipi->number = mapaddip["Number"].toInt();
                ipi->info = mapaddip["Info"].toString();

                //услуги страховой программы
                foreach (QVariant listofservices, result["Services"].toList()){
                    QVariantMap mapservices = listofservices.toMap();

                    si = new Service_info;
                    si->number = result["Number"].toInt();
                    si->name = result["Name"].toString();
                    si->cost = result["Cost"].toInt();
                    si->info = result["Info"].toString();
                    ipi->ArrOfServices.push_back(si);
                }

                //полюсы страховой программы
                foreach (QVariant listofpolicys, result["Policys"].toList()){
                    QVariantMap mappolisys = listofpolicys.toMap();

                    pi = new Policy_info;
                    pi->number = result["Number"].toString();
                    pi->patient = result["Patient"].toString();
                    pi->IC_program = result["IC number"].toInt();
                    pi->TillDate = QDate::fromString(result["Number"].toString(),"dd.MM.yyyy");
                    ipi->ArrOfPolicys.push_front(pi);
                }
                coni->DellListOfInsurancePrograms.push_back(ipi);
          }
            contract->ArrOfCondition.push_back(coni);
        }

        ListOfContractsInfo[contract->number] = contract;
    }

    bson_destroy (query);
    mongoc_cursor_destroy (cursor);

    return ListOfContractsInfo;
}

Contract *DataBase_Component::GetContract(int number){

    Contract *contract;
    Emploee *manager;
    InsuranceCompany *ic;
    InsuranceProgram *ip;
    Service *service;
    Policy *policy;
    Condition *condition;
    Clinic_Personal_Component CPrC;

    QDate FromDate, ToDate;

    QJson::Parser parser;
    QVariantMap result;
    bool ok;

    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;

    query = bson_new();
    BSON_APPEND_INT64(query, "Number", number);
    cursor = mongoc_collection_find(Contract_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    while (mongoc_cursor_next (cursor, &doc)) {
      result = parser.parse(bson_as_json (doc, NULL), &ok).toMap();
      ic = this->GetInsuranceCompany(result["Insurers company"].toString());
      manager = CPrC.GetManager(result["Manager number"].toInt());
      FromDate = QDate::fromString(result["From date"].toString(), "dd.MM.yyyy");
      ToDate = QDate::fromString(result["To date"].toString(), "dd.MM.yyyy");
      contract = new Contract (result["Number"].toInt(),
                                manager,
                                ic,
                                FromDate,
                                ToDate,
                                result["Info"].toString());

      //страховые программы
      foreach (QVariant listofip, result["Insurers programs"].toList()){
          QVariantMap mapip = listofip.toMap();

          ip = new InsuranceProgram(mapip["Number"].toInt(), mapip["Info"].toString());

          //услуги страховой программы
          foreach (QVariant listofservices, result["Services"].toList()){
              QVariantMap mapservices = listofservices.toMap();

              service = new Service(result["Number"].toInt(),
                                    result["Name"].toString(),
                                    result["Info"].toString(),
                                    result["Cost"].toInt());

              ip->AddService(service);
          }

          //полисы страховой программы
          foreach(QVariant listofpolicys, result["Policys"].toList()){
              QVariantMap mappolisys = listofpolicys.toMap();

              policy = new Policy(result["Number"].toString(),
                                  result["Patient"].toString(),
                                  QDate::fromString(result["Number"].toString(),"dd.MM.yyyy"),
                                  result["IC number"].toInt());

              ip->IssuePolicy(policy);
          }
          contract->AddInsuranceProgram(ip);
      }

      //дополнительные соглашанеия
      foreach (QVariant listofcondition, result["Conditions"].toList()){
          QVariantMap mapip = listofcondition.toMap();

          condition = new Condition(result["Number"].toInt(),
                                    result["Contract number"].toInt(),
                                    result["Info"].toString(),
                                    CPrC.GetManager(result["Manager number"].toInt()),
                                    QDate::fromString(result["To date"].toString(), "dd,MM,yyyy"));

          //новые страховые программы
          foreach (QVariant listofaddip, result["Add insurers programs"].toList()){
              QVariantMap mapaddip = listofaddip.toMap();

              ip = new InsuranceProgram(mapaddip["Number"].toInt(), mapaddip["Info"].toString());

              //услуги страховой программы
              foreach (QVariant listofservices, result["Services"].toList()){
                  QVariantMap mapservices = listofservices.toMap();

                  service = new Service(result["Number"].toInt(),
                                        result["Name"].toString(),
                                        result["Info"].toString(),
                                        result["Cost"].toInt());

                  ip->AddService(service);
              }

              //полисы страховой программы
              foreach(QVariant listofpolicys, result["Policys"].toList()){
                  QVariantMap mappolisys = listofpolicys.toMap();

                  policy = new Policy(result["Number"].toString(),
                                      result["Patient"].toString(),
                                      QDate::fromString(result["Number"].toString(),"dd.MM.yyyy"),
                                      result["IC number"].toInt());

                  ip->IssuePolicy(policy);
              }
              condition->AddToAddListOfInsuranceProgram(ip);
          }


          //удаляемые страховые программы
          foreach (QVariant listofdellip, result["Add insurers programs"].toList()){
              QVariantMap mapdellip = listofdellip.toMap();

              ip = new InsuranceProgram(mapdellip["Number"].toInt(), mapdellip["Info"].toString());

              //услуги страховой программы
              foreach (QVariant listofservices, result["Services"].toList()){
                  QVariantMap mapservices = listofservices.toMap();

                  service = new Service(result["Number"].toInt(),
                                        result["Name"].toString(),
                                        result["Info"].toString(),
                                        result["Cost"].toInt());

                  ip->AddService(service);
              }

              //полисы страховой программы
              foreach(QVariant listofpolicys, result["Policys"].toList()){
                  QVariantMap mappolisys = listofpolicys.toMap();

                  policy = new Policy(result["Number"].toString(),
                                      result["Patient"].toString(),
                                      QDate::fromString(result["Number"].toString(),"dd.MM.yyyy"),
                                      result["IC number"].toInt());

                  ip->IssuePolicy(policy);
              }
              condition->AddToDellListOfInsuranceProgram(ip);
          }
      }


      bson_destroy (query);
      mongoc_cursor_destroy (cursor);

      return contract;
    }

    bson_destroy (query);
    mongoc_cursor_destroy (cursor);
    throw std::exception();
}

QMap<QString, InsuranceCompany_info *> DataBase_Component::GetListOfInsuranceCompanyinfo(){
    QMap<QString, InsuranceCompany_info *> ListOfInsuranceCompanyinfo;
    InsuranceCompany_info *ici;

    QJson::Parser parser;
    QVariantMap result;
    bool ok;

    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;

    query = bson_new ();
    cursor = mongoc_collection_find(Company_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    while (mongoc_cursor_next (cursor, &doc)) {
        result = parser.parse(bson_as_json (doc, NULL), &ok).toMap();
        ici = new InsuranceCompany_info;
        ici->name = result["Name"].toString();
        ici->address =  result["Address"].toString();
        ici->phonenumber =  result["Phonenumber"].toString();
        ListOfInsuranceCompanyinfo[ici->name] = ici;
    }

    bson_destroy (query);
    mongoc_cursor_destroy (cursor);

    return ListOfInsuranceCompanyinfo;
}

InsuranceCompany *DataBase_Component::GetInsuranceCompany(QString name){
    InsuranceCompany *ic;

    QJson::Parser parser;
    QVariantMap result;
    bool ok;

    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;


    query = bson_new();
    BSON_APPEND_UTF8(query, "Name", name.toUtf8());
    cursor = mongoc_collection_find(Company_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    while (mongoc_cursor_next (cursor, &doc)) {
      result = parser.parse(bson_as_json (doc, NULL), &ok).toMap();
      ic = new InsuranceCompany(result["Name"].toString(),
                                result["Address"].toString(),
                                result["Phonenumber"].toString());
      bson_destroy (query);
      mongoc_cursor_destroy (cursor);
      return ic;
    }

    bson_destroy (query);
    mongoc_cursor_destroy (cursor);
    throw std::exception();
}

bool DataBase_Component::IsExistInsuranceCompany(QString name){
    mongoc_cursor_t *cursor;
    const bson_t *doc;
    bson_t *query;

    query = bson_new();
    BSON_APPEND_UTF8(query, "Name", name.toUtf8());
    cursor = mongoc_collection_find(Company_collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    while (mongoc_cursor_next (cursor, &doc)) {
      bson_destroy (query);
      mongoc_cursor_destroy (cursor);
      return true;
    }

    bson_destroy (query);
    mongoc_cursor_destroy (cursor);
    return false;
}
