#ifndef BUSINESS_COMPONENT_H
#define BUSINESS_COMPONENT_H

#include <QDate>
#include <QString>
#include "class_clinic_insurers.h"
#include "gateway.h"


class Business_component
{
public:
    Business_component(DataBase_Component* database_component,                          //шлюз обращения к базе
                       Clinic_Patient_Component* clinic_patient,                        //шлюз компонента работы с пациентами
                       Clinic_Personal_Component *clinic_personal);                     //шлюз компонента работы с персоналом

    void AddNewContract(Contract_info* Ci);                                             //добавление контракта
    void AddNewCondition(Condition_info* Cni);                                          //добавление дополнительного соглашения
    void AddNewInsuranceCompany(InsuranceCompany_info* ICi);                            //добавление новой страховой компании
    void UpdateInsuranceCompany(QString name, QString address, QString phonenumber);    //обновить данные страховой компании
    bool ViewServiceAvailability(int PatientNumber, int SNumber);                       //проверка доступности услуг
    void TerminateContract(int contract_number);                                        //Разорвать контракт


private:    
    InsuranceProgram *ConvertIPinfoToClass(InsuranceProgram_info *IPi);
    Condition *ConvertCninfoToClass(Condition_info *Cni);

    DataBase_Component* DBC;
    Clinic_Patient_Component* CPtC;
    Clinic_Personal_Component* CPrC;
};

#endif // BUSINESS_COMPONENT_H
