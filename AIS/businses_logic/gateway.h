#ifndef GATEWAY_H
#define GATEWAY_H

#include <QDate>
#include <QString>
#include <QDate>
#include <QVector>
#include <QDebug>
#include <mongoc.h>
#include <bson.h>
#include <stdio.h>
#include <stdlib.h>
#include "class_clinic_insurers.h"
#include <qjson-qt5/parser.h>
#include <time.h>



//информация менеджера
struct Manager_info{
    int number;
    QString FIO;
    QString position;
};

//информация полиса
struct Policy_info{
    QString number;
    QString patient;
    QDate TillDate;
    int IC_program;
};

//информация пациента
struct Patient_info{
    int number;
    //массив полисов
    QVector<Policy_info> ArrOfPolicys;
    QString FIO;
    QString passport;
};

//информация страховой компании
struct InsuranceCompany_info{
    QString name;
    QString address;
    QString phonenumber;
};

//информация услуги
struct Service_info{
    int number;
    QString name;
    QString info;
    int cost;
};

//информация страховой программы
struct InsuranceProgram_info{
    int number;
    //массив услуг
    QVector<Service_info*> ArrOfServices;
    //массив полисов
    QVector<Policy_info*> ArrOfPolicys;
    QString info;
};

struct Condition_info{
    int number;
    int contract_number;
    int manager_number;
    QDate ToDate;
    QString info;
    //массив добавленых программ страхования
    QVector<InsuranceProgram_info*> AddListOfInsurancePrograms;
    //массив удаленных программ страхования
    QVector<InsuranceProgram_info*> DellListOfInsurancePrograms;
};

//информация контракта
struct Contract_info{
    int number;
    int manager_number;
    InsuranceCompany_info *ICi;
    QDate FromDate;
    QDate ToDate;
    QString info;
    //массив программ
    QVector<InsuranceProgram_info*> ArrOfInsuranceProgram;
    //массив дополнительных соглашений
    QVector<Condition_info*> ArrOfCondition;
    QString status;
};



class Clinic_Patient_Component{
public:
    Clinic_Patient_Component();
    Patient *GetPatient(int number);                                            //получить информацию о пациенте
};



class Clinic_Personal_Component{
public:
    Clinic_Personal_Component();
    Emploee *GetManager(int number);                                              //получить информацию о менеджере
};




class DataBase_Component
{
public:
    DataBase_Component();
    void SaveContract(Contract *contract);                                     //сохранить контракт
    void UpdateContract(Contract *contract);
    void UpdateContract(Contract *contract, Condition *condition);//сохранить дополнительное соглашение
    void SaveInsuranceCompany(InsuranceCompany *insurancecompany);             //сохранить страховую компанию

    QMap<int, Contract_info *> GetListOfContractsInfo();
    Contract *GetContract(int number);                                          //получить контракт
    QMap<QString, InsuranceCompany_info*> GetListOfInsuranceCompanyinfo();               //получение списка страховых компаний
    InsuranceCompany* GetInsuranceCompany(QString name);                        //получить страховую компанию
//    InsuranceProgram* GetInsuranceProgram(int number);                          //получить программу страхования

    bool IsExistInsuranceCompany(QString name);                                 //проверка на существование страховой компании


private:
    mongoc_collection_t *Company_collection, *Contract_collection;
    mongoc_client_t *client;

};


#endif // GATEWAY_H
