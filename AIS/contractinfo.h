#ifndef CONTRACTINFO_H
#define CONTRACTINFO_H

#include <QDialog>
#include <QString>
#include <QDate>
#include "addip.h"
#include "addcondition.h"

namespace Ui {
class ContractInfo;
}

class ContractInfo : public QDialog
{
    Q_OBJECT

public:
    explicit ContractInfo(Contract_info Ci,QWidget *parent = 0);
    ~ContractInfo();

signals:
    void newcondition(Condition_info *Coni);
    void TerminateContract(Contract_info *Ci);

private slots:
    void on_pushButton_3_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void newConditioninfo(Condition_info *Coni);

private:
    Ui::ContractInfo *ui;

    Contract_info Ci;
    AddCondition *addCon;
};

#endif // CONTRACTINFO_H
