#include "addic.h"
#include "ui_addic.h"

AddIC::AddIC(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddIC)
{
    ui->setupUi(this);
    ui->lineEdit_3->setInputMask("+7(000)-000-00-00");
}

AddIC::~AddIC()
{
    delete ui;
}

void AddIC::on_pushButton_2_clicked()
{
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->lineEdit_3->clear();
    this->close();
}

void AddIC::on_pushButton_clicked()
{
    try{
        if(ui->lineEdit->text() == "" || ui->lineEdit_2->text() == "" || ui->lineEdit_3->text() == ""){
            throw std::exception();
        }
        ICi = new InsuranceCompany_info;
        ICi->name = ui->lineEdit->text();
        ICi->address = ui->lineEdit_2->text();
        ICi->phonenumber = ui->lineEdit_3->text();

        emit newInsuranceCompanyinfo(ICi);
        ui->lineEdit->clear();
        ui->lineEdit_2->clear();
        ui->lineEdit_3->clear();
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Проверьте правильность данных");
    }
}
