#ifndef VIEWLISTOFIC_H
#define VIEWLISTOFIC_H

#include <QDialog>
#include "businses_logic/gateway.h"

namespace Ui {
class ViewListOfIC;
}

class ViewListOfIC : public QDialog
{
    Q_OBJECT

public:
    explicit ViewListOfIC(QMap<QString, InsuranceCompany_info*> ListOfInsuranceCompanyinfo, QWidget *parent = 0);
    ~ViewListOfIC();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ViewListOfIC *ui;
};

#endif // VIEWLISTOFIC_H
