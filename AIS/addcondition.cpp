#include "addcondition.h"
#include "ui_addcondition.h"

AddCondition::AddCondition(Contract_info Ci, QWidget *parent) :
    Ci(Ci), QDialog(parent),
    ui(new Ui::AddCondition)
{
    ui->setupUi(this);

    ui->lineEdit->setValidator(new QRegExpValidator(QRegExp("^([1-9][0-9]*|0)"),this));

    Coni = new Condition_info;
    Coni->contract_number = Ci.number;
    this->setWindowTitle(this->windowTitle() + QString::number(Ci.number));
    ui->dateEdit->setDate(Ci.ToDate);
    ui->textEdit->setText(Ci.info);
    for(int i=0; i< Ci.ArrOfInsuranceProgram.size(); i++){
        ui->comboBox->addItem(QString::number(Ci.ArrOfInsuranceProgram[i]->number));
    }
}

AddCondition::~AddCondition()
{
    delete ui;
}

void AddCondition::on_pushButton_3_clicked()
{
    addIP = new AddIP();
    connect(addIP, SIGNAL(newInsurancePrograminfo(InsuranceProgram_info*)), this, SLOT(newInsurancePrograminfo(InsuranceProgram_info*)));
    addIP->exec();
}

void AddCondition::on_pushButton_clicked()
{
    this->close();
}

void AddCondition::on_pushButton_4_clicked()
{
    for(int i=0; i< ui->listWidget_2->count(); i++)
        if(ui->listWidget_2->item(i)->text() == ui->comboBox->currentText())

    for (int ipi=0; ipi<Ci.ArrOfInsuranceProgram.size(); ipi++)
        if (Ci.ArrOfInsuranceProgram[ipi]->number == ui->comboBox->currentText().toInt())
            Coni->DellListOfInsurancePrograms.push_back(Ci.ArrOfInsuranceProgram[ipi]);

    InsuranceProgram_info *ipi = new InsuranceProgram_info;
    ipi->number = ui->comboBox->currentText().toInt();

    ui->listWidget_2->addItem(ui->comboBox->currentText());
    ui->comboBox->removeItem(ui->comboBox->currentIndex());

    Coni->DellListOfInsurancePrograms.push_back(ipi);
}

void AddCondition::newInsurancePrograminfo(InsuranceProgram_info *IPi){
    try{
        for(int i=0; i< ui->listWidget->count(); i++)
            if(ui->listWidget->item(i)->text().toInt() == IPi->number)
                throw std::exception();

        Coni->AddListOfInsurancePrograms.push_back(IPi);
        ui->listWidget->addItem(QString::number(IPi->number));
        addIP->close();
    }catch(...){
        QMessageBox::warning(addIP,"Ошибка", "Услуга с таким номером уже существует");
    }
}

void AddCondition::on_pushButton_2_clicked()
{
    try{
        if(ui->lineEdit->text() == "" || ui->dateEdit->date() < Ci.FromDate){
            throw std::exception();
        }
        Coni->number = ui->lineEdit->text().toInt();
        Coni->contract_number = Ci.number;
        Coni->ToDate = ui->dateEdit->date();
        Coni->info = ui->textEdit->toPlainText();

        emit newConditioninfo(Coni);
    }catch(...){
        QMessageBox::warning(this,"Ошибка", "Проверьте правильность данных");
    }
}
