QT += core
QT -= gui

QMAKE_CXXFLAGS += -std=c++0x

CONFIG += c++11

CONFIG += qtestlib
TARGET = ../TestLib

SOURCES += \
    class_clinic_insurers.cpp \
    business_component.cpp \
    gateway.cpp \
    test.cpp

HEADERS += \
    class_clinic_insurers.h \
    business_component.h \
    gateway.h

CONFIG += qtestlib
TARGET = ../TestLib

