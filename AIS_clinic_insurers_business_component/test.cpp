#include <QtTest>
#include <QDate>
#include <QObject>
#include <typeinfo>
#include "business_component.h"


// ======================================================================
class Test : public QObject {
    Q_OBJECT
private:
    DataBase_Component DBC;
    Clinic_Patient_Component CPnC;
    Clinic_Personal_Component CPrC;


private slots:
    void AddContract();
    void AddCondition();
    void AddNewInsuranceCompany();
    void UpdateInsuranceCompany();
    void ViewServiceAvailability();
};

// ----------------------------------------------------------------------


void Test::AddContract(){
    Business_component BC(&DBC,&CPnC,&CPrC);


    //удачное добавление
    InsuranceCompany_info ICi;
    ICi.name = "New company";
    ICi.address = "City, Street, №";
    ICi.phonenumber = "+7(911)-222-33-44";

    QDate FromDate(2018,12,05), ToDate(2018,12,31);
    Contract_info Ci;
    Ci.number = 1;
    Ci.manager_number = 764583;
    Ci.ICi = ICi;
    Ci.info = "First contract";
    Ci.FromDate = FromDate;
    Ci.ToDate = ToDate;
    Ci.status = "New";

    BC.AddNewContract(&Ci);


    //контракт с номером 0 существует
    Ci.number = 999;
    QVERIFY_EXCEPTION_THROWN(BC.AddNewContract(&Ci), std::exception);


    //неудачное добавление
    Ci.FromDate = ToDate;
    Ci.ToDate = FromDate;
    Ci.number = 2;

    QVERIFY_EXCEPTION_THROWN(BC.AddNewContract(&Ci), std::exception);
}

void Test::AddCondition(){
    Business_component BC(&DBC,&CPnC,&CPrC);

    //дополнительное соглашение по несуществующему контракту
    QDate ToDate(2019,01,01);
    Condition_info Cni;
    Cni.number = 111;
    Cni.contract_number = 1;
    Cni.manager_number = 764583;
    Cni.ToDate = ToDate;
    Cni.info = "Condition for contract №" + QString::number(Cni.number);

    QVERIFY_EXCEPTION_THROWN(BC.AddNewCondition(&Cni), std::exception);


    //успешное добавление
    Cni.contract_number = 999;

    BC.AddNewCondition(&Cni);


    //новая дата < текущей
    ToDate.setDate(2018,12,01);
    Cni.ToDate = ToDate;
    Cni.number = 2;

    QVERIFY_EXCEPTION_THROWN(BC.AddNewCondition(&Cni), std::exception);
}

void Test::AddNewInsuranceCompany(){
    Business_component BC(&DBC,&CPnC,&CPrC);

    //повторное добавление страховой компании
    InsuranceCompany_info ICi;
    ICi.name = "Old company";
    ICi.address = "City, Street, №";
    ICi.phonenumber = "+7(909)-888-00-00";

    QVERIFY_EXCEPTION_THROWN(BC.AddNewInsuranceCompany(&ICi), std::exception);


    //успешное добавление
    ICi.name = "New company";

    BC.AddNewInsuranceCompany(&ICi);
}

void Test::UpdateInsuranceCompany(){
    Business_component BC(&DBC,&CPnC,&CPrC);

    //обновление информации по несуществующей компании
    QVERIFY_EXCEPTION_THROWN(BC.UpdateInsuranceCompany("Imagen company","Address","Phonenumber"), std::exception);


    //удачное обновление только адреса
    BC.UpdateInsuranceCompany("Old company","New address","");


    //удачное обновление только телефона
    BC.UpdateInsuranceCompany("Old company","","+7(909)-888-88-88");


    //удачное обновление
    BC.UpdateInsuranceCompany("Old company","New address","+7(909)-888-88-88");
}

void Test::ViewServiceAvailability(){
    Business_component BC(&DBC,&CPnC,&CPrC);

    //действующий полис с доступной услугой
    QCOMPARE(BC.ViewServiceAvailability(400,88800101), true);


    //действующий полис с недоступной услугой
    QCOMPARE(BC.ViewServiceAvailability(400,88800104), false);


    //не действующий полис с доступной услугой
    QCOMPARE(BC.ViewServiceAvailability(400,99900101), false);
}


QTEST_MAIN(Test)
#include "test.moc"
