#include "business_component.h"


Business_component::Business_component(DataBase_Component* database_component,
                                       Clinic_Patient_Component *clinic_patient,
                                       Clinic_Personal_Component *clinic_personal)
    : DBC(database_component), CPtC(clinic_patient), CPrC(clinic_personal)
{}

InsuranceProgram *Business_component::ConvertIPinfoToClass(InsuranceProgram_info* IPi){
    InsuranceProgram *IP = new InsuranceProgram(IPi->number, IPi->info);

    //цикл по услугам
    for(int s=0; s< IPi->ArrOfServices.size(); s++){
        Service *Sr = new Service(IPi->ArrOfServices[s].number,
                                  IPi->ArrOfServices[s].name,
                                  IPi->ArrOfServices[s].info,
                                  IPi->ArrOfServices[s].cost);
        IP->AddService(Sr);
    }

    //цикл по полисам
    for(int p=0; p<IPi->ArrOfPolicys.size(); p++){
        Policy *Pl = new Policy(IPi->ArrOfPolicys[p].number,
                                IPi->ArrOfPolicys[p].patient,
                                IPi->ArrOfPolicys[p].TillDate,
                                IPi->number);
        IP->IssuePolicy(Pl);
    }
    return IP;
}


Condition *Business_component::ConvertCninfoToClass(Condition_info *Cni){
    Emploee *manager = CPrC->GetManager(Cni->manager_number);

    Condition *condition = new Condition(Cni->number,
                                Cni->contract_number,
                                Cni->info,
                                manager,
                                Cni->ToDate);

    //цикл по добаленным страховым программам
    for(int a=0; a<Cni->AddListOfInsurancePrograms.size(); a++)
        condition->AddToAddListOfInsuranceProgram(ConvertIPinfoToClass(&(Cni->AddListOfInsurancePrograms[a])));

    //цикл по удаленным страховым программам
    for (int d=0; d<Cni->DellListOfInsurancePrograms.size(); d++)
        condition->AddToDellListOfInsuranceProgram(ConvertIPinfoToClass(&(Cni->DellListOfInsurancePrograms[d])));

    return condition;
}

void Business_component::AddNewContract(Contract_info *Ci){
    try {
        DBC->GetContract(Ci->number);
    } catch (...) {
        Emploee *manager = CPrC->GetManager(Ci->manager_number);

        InsuranceCompany IC(Ci->ICi.name, Ci->ICi.address, Ci->ICi.phonenumber);
        Contract contract(Ci->number, manager, &IC, Ci->FromDate, Ci->ToDate, Ci->info);

        //цикл по страховым программам
        for(int i=0; i< Ci->ArrOfInsuranceProgram.size(); i++){
            contract.AddInsuranceProgram(ConvertIPinfoToClass(&(Ci->ArrOfInsuranceProgram[i])));
        }

        DBC->SaveContract(&contract);
        return;
    }
    throw std::exception();
}




void Business_component::AddNewCondition(Condition_info* Cni){

//--------------------------------------------------------сборка класса контракта--------------------------------------------------------
    Contract *contract = DBC->GetContract(Cni->contract_number);



//-----------------------------------------------------проверка корректности данных------------------------------------------------------
    if ((contract->GetToDate() < QDate::currentDate()) || (contract->GetStatus() == "Terminated") || (Cni->ToDate < contract->GetToDate()))
        throw std::exception();
    else{


//---------------------------------------------------сборка дополнительного соглашения---------------------------------------------------
        Emploee *CManager = CPrC->GetManager(Cni->manager_number);
        Condition newCondition(Cni->number, Cni->contract_number, Cni->info, CManager, Cni->ToDate);

        //цикл по добаленным страховым программам
        for(int a=0; a<Cni->AddListOfInsurancePrograms.size(); a++)
            newCondition.AddToAddListOfInsuranceProgram(ConvertIPinfoToClass(&(Cni->AddListOfInsurancePrograms[a])));

        //цикл по удаленным страховым программам
        for (int d=0; d<Cni->DellListOfInsurancePrograms.size(); d++)
            newCondition.AddToDellListOfInsuranceProgram(ConvertIPinfoToClass(&(Cni->DellListOfInsurancePrograms[d])));

        contract->AddCondition(&newCondition);
        DBC->SaveCondition(&newCondition);
    }


//----------------------------------------------------------------------------------------------------------------------------------------

    DBC->SaveContract(contract);
}

void Business_component::AddNewInsuranceCompany(InsuranceCompany_info* ICi){
    if (DBC->IsExistInsuranceCompany(ICi->name))
        throw std::exception();
    else{
        InsuranceCompany IC(ICi->name, ICi->address, ICi->phonenumber);
        DBC->SaveInsuranceCompany(&IC);
    }
}

void Business_component::UpdateInsuranceCompany(QString name, QString address, QString phonenumber){
    InsuranceCompany *IC = DBC->GetInsuranceCompany(name);

    if (address !="")
        IC->UpdateAddress(address);

    if (phonenumber != "" )
        IC->UpdatePhonenumber(phonenumber);

    DBC->SaveInsuranceCompany(IC);
}


bool Business_component::ViewServiceAvailability(int PatientNumber, int SNumber){
    Patient *patient = CPtC->GetPatient(PatientNumber);

    QMap<QString, Policy*> ListOfPolicys = patient->GetPolicys();
    QMap<QString, Policy*>::iterator it = ListOfPolicys.begin();

//    for (auto police : ListOfPolicys)
//    {

//    }

    for(;it != ListOfPolicys.end(); ++it){
        if(it.value()->isValid()){
            InsuranceProgram *IP = DBC->GetInsuranceProgram(it.value()->GetInsuranceProgramNumber());
            QMap<int, Service *> ListOfServices = IP->GetServices();
            if(ListOfServices.contains(SNumber)){
                delete(IP);
                return true;
            }
            delete(IP);
        }
    }
    return false;
}
