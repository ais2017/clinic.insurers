#include "gateway.h"


Clinic_Patient_Component::Clinic_Patient_Component(){

}

Patient *Clinic_Patient_Component::GetPatient(int number){
    if (number == 400){
        QDate TillDate(2018,12,01);
        Policy *Pl_400_1 = new Policy("99900140002", "F400 I400 O400", TillDate, 999001);
        Policy *Pl_400_2 = new Policy("88800140001", "F400 I400 O400", QDate::currentDate().addYears(1), 888001);

        Patient *P_400 = new Patient(400, "F400 I400 O400", "66 66 098245");
        P_400->AddPolicy(Pl_400_1);
        P_400->AddPolicy(Pl_400_2);

        return P_400;
    }

    throw std::exception();
}




Clinic_Personal_Component::Clinic_Personal_Component(){

}


Emploee *Clinic_Personal_Component::GetManager(int number){
    Emploee *manager = new Emploee(number, "Manager FIO", "Manager");
    return manager;
}




DataBase_Component::DataBase_Component::DataBase_Component(){

}

void DataBase_Component::SaveContract(Contract *contract){
    qDebug() << "\n-----------------------------------------------------------" <<
                "\nContract №" << contract->GetNumber() << " save sucsess" <<
                "\nContract manager: " << contract->GetManager()->GetNumber() <<
                "\nContract FromDate: " << contract->GetFromDate() <<
                "\nContract ToDate: " << contract->GetToDate() <<
                "\nContract insurance company: " <<contract->GetInsuranceCompany()->GetName() <<
                "\nContract info: " << contract->GetInfo() <<
                "\n-----------------------------------------------------------";
}

void DataBase_Component::SaveCondition(Condition *condition){
    qDebug() << "\n-----------------------------------------------------------" <<
                "\nCondition №" << condition->GetNumber() << " save sucsess" <<
                "\nCondition manager: " << condition->GetManager()->GetNumber() <<
                "\nCondition contract number: " << condition->GetContractNumber() <<
                "\nCondition ToDate: " << condition->GetToDate() <<
                "\nCondition info: " << condition->GetInfo() <<
                "\n-----------------------------------------------------------";
}

void DataBase_Component::SaveInsuranceCompany(InsuranceCompany* insurancecompany){
    qDebug() << "\n-----------------------------------------------------------" <<
                "\nCompany " << insurancecompany->GetName() << " save sucsess" <<
                "\nCompany address: " << insurancecompany->GetAddress() <<
                "\nCompany phonenumber: " << insurancecompany->GetPhonenumber() <<
                "\n-----------------------------------------------------------";

}

Contract *DataBase_Component::GetContract(int number){
    if(number == 999){
        Clinic_Personal_Component CPrC;
        Emploee *manager = CPrC.GetManager(764583);

        InsuranceCompany *IC = new InsuranceCompany("Old company", "City, Street, №", "+7(911)-222-33-44");

        QDate FromDate(2018,12,05), ToDate(2018,12,31);
        Contract *C = new Contract(999, manager, IC, FromDate , ToDate, "First contract");

        return C;
    }

    if(number == 888){
        Clinic_Personal_Component CPrC;
        Emploee *manager = CPrC.GetManager(864584);

        InsuranceCompany *IC = new InsuranceCompany("Old old company", "City, Street, №", "+7(911)-822-83-84");

        QDate FromDate(2010,12,01), ToDate(2020,12,31);
        Contract *C = new Contract(888, manager, IC, FromDate , ToDate, "Second contract");

        return C;
    }

    throw std::exception();
}

QMap<QString, InsuranceCompany *> DataBase_Component::GetListOfInsuranceCompanyInformation(){


}

InsuranceCompany *DataBase_Component::GetInsuranceCompany(QString name){
    if (name == "Old company"){
        InsuranceCompany *IC = new InsuranceCompany("Old company", "City, Street, №", "+7(911)-222-33-44");
        return IC;
    }

    if (name == "Old old company"){
        InsuranceCompany *IC = new InsuranceCompany("Old old company", "City, Street, №", "+7(911)-822-83-84");
        return IC;
    }

    throw std::exception();
}

InsuranceProgram *DataBase_Component::GetInsuranceProgram(int number){
    if (number == 999001){
        //действительный полис для пациента 500
        QDate TillDate;
        Policy *Pl_999001_2 = new Policy("99900150001", "F500 I500 O500", QDate::currentDate().addYears(1), 999001);

        //полис с истекшей датой для пациента 400
        TillDate.setDate(2018,12,01);
        Policy *Pl_999001_1 = new Policy("99900140002", "F400 I400 O400", TillDate, 999001);

        //услуги страховой програ
        Service *S_999001_1 = new Service(99900101, "Service one", "Important info about service", 200);
        Service *S_999001_2 = new Service(99900102, "Service two", "Important info about service", 15000);

        InsuranceProgram *IP = new InsuranceProgram(999001, "Insurance program №999001");
        IP->AddService(S_999001_1);
        IP->AddService(S_999001_2);

        IP->IssuePolicy(Pl_999001_1);
        IP->IssuePolicy(Pl_999001_2);

        return IP;
    }

    if (number == 888001){
        //действительный полис для пациента 400
        Policy *Pl_888001_1 = new Policy("88800140001", "F400 I400 O400", QDate::currentDate().addYears(1), 888001);

        //действительный полис для пациента 600
        Policy *Pl_888001_2 = new Policy("88800160002", "F600 I600 O600", QDate::currentDate().addYears(2), 888001);

        //услуги страховой програ
        Service *S_888001_1 = new Service(88800101, "Service four", "Important info about service", 900);
        Service *S_888001_2 = new Service(88800102, "Service five", "Important info about service", 66666);

        InsuranceProgram *IP = new InsuranceProgram(888001, "Insurance program №888001");
        IP->AddService(S_888001_1);
        IP->AddService(S_888001_2);

        IP->IssuePolicy(Pl_888001_1);
        IP->IssuePolicy(Pl_888001_2);

        return IP;
    }

    throw std::exception();
}

bool DataBase_Component::IsExistInsuranceCompany(QString name){
    if (name == "Old company")
        return true;

    return false;
}
